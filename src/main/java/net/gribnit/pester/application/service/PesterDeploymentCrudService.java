package net.gribnit.pester.application.service;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.gribnit.pester.domain.definitions.DeployedPesterDefinition;
import net.gribnit.pester.domain.definitions.GeneratorDefinition;
import net.gribnit.pester.domain.definitions.PesterDefinition;
import net.gribnit.pester.domain.definitions.SchedulerDefinition;
import net.gribnit.pester.domain.definitions.UserDefinition;
import net.gribnit.pester.exception.CreationConflictException;
import net.gribnit.pester.exception.ModificationConflictException;
import net.gribnit.pester.exception.NotFoundException;
import net.gribnit.pester.persistence.repositories.DeployedPesterRepository;
import net.zethmayr.benjamin.spring.common.mapper.base.Mapper;
import net.zethmayr.benjamin.spring.common.repository.base.RepositoryException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Clock;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static net.gribnit.pester.persistence.PersistenceNames.GENERATOR_NAME;
import static net.gribnit.pester.persistence.PersistenceNames.SCHEDULER_NAME;
import static net.gribnit.pester.persistence.PersistenceNames.USER_ID;

@Service
@Slf4j
public class PesterDeploymentCrudService {

    private final PesterUsersCrudService users;
    private final DeployedPesterRepository deployedPesters;
    private final PesterEvents pesterEvents;
    private final String selectByUserId;
    private final String selectByScheduler;
    private final String selectByGenerator;
    private final Clock theClock;

    public PesterDeploymentCrudService(
            final @Autowired PesterUsersCrudService users,
            final @Autowired DeployedPesterRepository deployedPesters,
            final @Autowired PesterEvents pesterEvents,
            final @Autowired Clock theClock
    ) {
        this.users = users;
        this.deployedPesters = deployedPesters;
        this.pesterEvents = pesterEvents;
        this.theClock = theClock;
        pesterEvents.listenUserDeleted(this::removeDeploymentsForUser);
        pesterEvents.listenSchedulerDeleted(this::removeDeploymentsForScheduler);
        pesterEvents.listenGeneratorDeleted(this::removeDeploymentsForGenerator);
        val pesterByUser = deployedPesters.findMapper(USER_ID);
        if (Objects.isNull(pesterByUser)) {
            throw RepositoryException.badSetup("No mapper for " + USER_ID);
        }
        selectByUserId = deployedPesters.select() + pesterByUser.whereClause();
        val pesterByScheduler = deployedPesters.findMapper(SCHEDULER_NAME);
        if (Objects.isNull(pesterByScheduler)) {
            throw RepositoryException.badSetup("No mapper for " + SCHEDULER_NAME);
        }
        selectByScheduler = deployedPesters.select() + pesterByScheduler.whereClause();
        val pesterByGenerator = deployedPesters.findMapper(GENERATOR_NAME);
        if (Objects.isNull(pesterByGenerator)) {
            throw RepositoryException.badSetup("No mapper for " + GENERATOR_NAME);
        }
        selectByGenerator = deployedPesters.select() + pesterByGenerator.whereClause();
    }

    public List<DeployedPesterDefinition> getAll() {
        return deployedPesters.getAll();
    }

    public List<PesterDefinition> getForUser(final String userName) throws NotFoundException {
        val user = users.getOne(userName);
        return deployedPesters.getUnsafe(selectByUserId, user.getId())
                .stream().map(d -> new PesterDefinition().from(d)).collect(Collectors.toList());
    }

    public void removeDeploymentsForUser(final UserDefinition deletedUser) {
        val userDeployments = deployedPesters.getUnsafe(selectByUserId, deletedUser.getId());
        userDeployments.forEach(this::deleteInternal);
    }

    public void removeDeploymentsForScheduler(final SchedulerDefinition deletedScheduler) {
        val schedulerDeployments = deployedPesters.getUnsafe(selectByScheduler, deletedScheduler.getName());
        schedulerDeployments.forEach(this::deleteInternal);
    }

    public void removeDeploymentsForGenerator(final GeneratorDefinition deletedGenerator) {
        val generatorDeployments = deployedPesters.getUnsafe(selectByGenerator, deletedGenerator.getName());
        generatorDeployments.forEach(this::deleteInternal);
    }

    public void delete(final int pesterId) throws NotFoundException {
        val found = deployedPesters.get(pesterId).orElseThrow(
                () -> NotFoundException.missing("Pester with id " + pesterId)
        );
        deleteInternal(found);
    }

    private void deleteInternal(final DeployedPesterDefinition toDelete) {
        pesterEvents.pesterDeleted(toDelete);
        deployedPesters.deleteMonadic(toDelete);
    }

    public DeployedPesterDefinition deploy(final String userName, final PesterDefinition pester) throws CreationConflictException, NotFoundException {
        LOG.trace("deploy(\n{},\n{}\n)", userName, pester);
        val user = users.getOne(userName);
        val resolved = new DeployedPesterDefinition()
                .from(pester)
                .setDeployed(Instant.now(theClock))
                .setUserId(user.getId());
        LOG.trace("resolved is {}", resolved);
        deployedPesters.insert(resolved);
        pesterEvents.pesterCreated(resolved);
        return resolved;
    }

    public DeployedPesterDefinition modify(int pesterId, PesterDefinition pester) throws ModificationConflictException, NotFoundException {
        LOG.trace("modify(\n{},\n{}\n)", pesterId, pester);
        return null;
    }
}
