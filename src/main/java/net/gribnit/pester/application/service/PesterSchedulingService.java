package net.gribnit.pester.application.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.gribnit.pester.domain.BasicScheduler;
import net.gribnit.pester.domain.definitions.BasicSchedulerDefinition;
import net.gribnit.pester.domain.definitions.DeployedPesterDefinition;
import net.gribnit.pester.domain.definitions.GeneratorDefinition;
import net.gribnit.pester.domain.definitions.SchedulerDefinition;
import net.gribnit.pester.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.stereotype.Service;

import java.time.Clock;
import java.time.Duration;
import java.time.Period;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAmount;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ScheduledFuture;
import java.util.regex.Pattern;

import static java.time.temporal.ChronoField.HOUR_OF_DAY;
import static java.time.temporal.ChronoField.MINUTE_OF_HOUR;
import static net.gribnit.pester.domain.BasicScheduler.TIMING;

@Service
@Slf4j
public class PesterSchedulingService {

    private final PesterDeploymentCrudService deployedPesters;
    private final PesterSchedulersCrudService schedulersCrud;
    private final PesterGeneratorsCrudService generatorsCrud;
    private final ThreadPoolTaskScheduler taskScheduler;
    private final Map<String, ScheduledFuture> scheduledTasks;
    private final Clock theClock;
    private final PesterDeliveryService pesterDelivery;

    @SuppressWarnings("unused") // IOC constructor
    public PesterSchedulingService(
            final @Autowired PesterEvents pesterEvents,
            final @Autowired PesterDeploymentCrudService deployedPesters,
            final @Autowired PesterSchedulersCrudService schedulersCrud,
            final @Autowired PesterGeneratorsCrudService generatorsCrud,
            final @Autowired ThreadPoolTaskScheduler taskExecutor,
            final @Autowired Clock theClock,
            final @Autowired PesterDeliveryService pesterDelivery
    ) {
        this.theClock = theClock;
        this.scheduledTasks = new HashMap<>();
        this.deployedPesters = deployedPesters;
        this.schedulersCrud = schedulersCrud;
        this.generatorsCrud = generatorsCrud;
        this.taskScheduler = taskExecutor;
        this.pesterDelivery = pesterDelivery;
        pesterEvents.listenPesterCreated(this::createSchedulerFor);
        pesterEvents.listenPesterDeleted(this::removeSchedulerFor);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void createExistingSchedules() {
        LOG.info("scheduling existing tasks");
        val deployed = deployedPesters.getAll();
        for (val each : deployed) {
            createSchedulerFor(each);
        }
    }

    private Optional<SchedulerDefinition> checkScheduler(final String schedulerName) {
        try {
            return Optional.ofNullable(schedulersCrud.getOne(schedulerName));
        } catch (NotFoundException nfe) {
            LOG.error("cannot schedule; no such scheduler", nfe);
            return Optional.empty();
        }
    }

    private Optional<GeneratorDefinition> checkDefaultGenerator(final String generatorName) {
        try {
            return Optional.ofNullable(generatorsCrud.getOne(generatorName));
        } catch (NotFoundException nfe) {
            LOG.error("cannot schedule; no such generator", nfe);
            return Optional.empty();
        }
    }

    private Optional<String> checkGeneratorName(final BasicSchedulerDefinition basic, final String defaultGeneratorName) {
        val generatorName = basic.getGenerator();
        if (!Objects.isNull(generatorName) && !defaultGeneratorName.equals(generatorName)) {
            if (!checkDefaultGenerator(generatorName).isPresent()) {
                return Optional.empty();
            }
        }
        return Optional.of(Objects.isNull(generatorName) ? defaultGeneratorName : generatorName);
    }

    private Optional<BasicScheduler> checkBasis(final BasicSchedulerDefinition basic) {
        val typeName = basic.getType();
        val basis = BasicScheduler.forName(basic.getType());
        if (Objects.isNull(basis)) {
            LOG.error("cannot schedule; no such type as " + typeName);
            return Optional.empty();
        }
        val params = basic.getParams();
        if (!basis.requiredParametersPresent(params)) {
            LOG.error("cannot schedule; missing required parameters");
            return Optional.empty();
        }
        return Optional.of(basis);
    }

    private static final Pattern AT = Pattern.compile("@");

    public void createSchedulerFor(final DeployedPesterDefinition pesterDefinition) {
        LOG.info("scheduling {}", pesterDefinition);
        val schedulerName = pesterDefinition.getScheduler();
        final SchedulerDefinition scheduler;
        scheduler = checkScheduler(schedulerName).orElse(null);
        if (Objects.isNull(scheduler)) {
            return;
        }
        val defaultGeneratorName = pesterDefinition.getGenerator();
        if (!checkDefaultGenerator(defaultGeneratorName).isPresent()) {
            return;
        }
        for (val each : scheduler.getSchedulers()) {
            val id = each.getId();
            val task = scheduledTasks.get(id);
            if (!Objects.isNull(task)) {
                LOG.warn("already scheduled {}, this shouldn't happen..", id);
                continue;
            }
            val typeName = each.getType();
            val generatorName = checkGeneratorName(each, defaultGeneratorName).orElse(null);
            if (Objects.isNull(generatorName)) {
                continue;
            }
            val basis = checkBasis(each).orElse(null);
            if (Objects.isNull(basis)) {
                continue;
            }
            val params = each.getParams();
            switch (basis) {
                case PERIODIC:
                    LOG.info("parameters are {}", params);
                    val rawTiming = (String) params.get(TIMING);
                    val atAt = rawTiming.indexOf("@");
                    if (atAt > 0) {
                        scheduleAtPeriod(pesterDefinition, each, rawTiming);
                    } else {
                        LOG.warn("cannot schedule; bad timing string {}", rawTiming);
                    }
                    break;
            }
        }
    }

    private void scheduleAtPeriod(final DeployedPesterDefinition deployedPester, final BasicSchedulerDefinition basicPester, final String rawTiming) {
        val timing = AT.split(rawTiming, 2);
        final String rawPeriod = timing[0].toLowerCase();
        switch (rawPeriod) {
            case "hourly":
                scheduleHourly(deployedPester, basicPester, timing[1]);
                break;
            case "daily":
                scheduleDaily(deployedPester, basicPester, timing[1]);
                break;
            case "weekly":
                scheduleWeekly(deployedPester, basicPester, timing[1]);
                break;
            default:
                LOG.warn("Unsupported period");
                break;
        }
    }

    private static final Pattern TWO_DIGITS = Pattern.compile("[0-9]{2}");

    private void scheduleHourly(final DeployedPesterDefinition deployedPester, final BasicSchedulerDefinition basicPester, final String rawMinutes) {
        // we expect timing[1] to be :MM or MM
        val period = Duration.ofHours(1);
        val minutes = minutesFrom(rawMinutes);
        if (minutes < 0) {
            LOG.warn("Cannot schedule, invalid timing");
            return;
        }
        val now = ZonedDateTime.now(theClock);
        ZonedDateTime then = now.with(MINUTE_OF_HOUR, minutes);
        if (then.isBefore(now)) {
            then = then.plus(period);
        }
        createPeriodicSchedulerInternal(deployedPester, basicPester, then, period);
    }

    static int minutesFrom(final String rawMinutes) {
        val minutesMatcher = TWO_DIGITS.matcher(rawMinutes);
        if (minutesMatcher.find()) {
            return Integer.valueOf(minutesMatcher.group());
        }
        return -1;
    }

    private void scheduleDaily(final DeployedPesterDefinition deployedPester, final BasicSchedulerDefinition basicPester, final String rawTiming) {
        // we expect timing[1] to be HH:MM or HH
        val period = Period.ofDays(1);
        val matcher = TWO_DIGITS.matcher(rawTiming);
        final int hourOfDay;
        if (matcher.find()) {
            hourOfDay = Integer.valueOf(matcher.group());
        } else {
            LOG.warn("Cannot schedule - no hour");
            return;
        }
        final int minuteOfHour;
        if (matcher.find()) {
            minuteOfHour = Integer.valueOf(matcher.group());
        } else {
            minuteOfHour = 0;
        }
        LOG.trace("Saw hourOfDay {}, minuteOfHour {}", hourOfDay, minuteOfHour);
        val now = ZonedDateTime.now(theClock);
        ZonedDateTime then = now.with(HOUR_OF_DAY, hourOfDay).with(MINUTE_OF_HOUR, minuteOfHour);
        if (then.isBefore(now)) {
            then = then.plus(period);
        }
        createPeriodicSchedulerInternal(deployedPester, basicPester, then, period);
    }

    private void scheduleWeekly(final DeployedPesterDefinition deployedPester, final BasicSchedulerDefinition basicPester, final String rawTiming) {
        // we expect timing[1] to be ddd HH:MM
        val period = Period.ofWeeks(1);
    }

    @AllArgsConstructor
    public static class TaskData {
        final int deployedId;
        final int basicId;
    }

    private String generateTaskId(final DeployedPesterDefinition deployed, final BasicSchedulerDefinition basic) {
        return "" + deployed.getId() + "-" + basic.getId();
    }

    private void createPeriodicSchedulerInternal(
            final DeployedPesterDefinition deployedPester,
            BasicSchedulerDefinition basicPester,
            final ZonedDateTime then,
            TemporalAmount period
    ) {
        val pesterId = generateTaskId(deployedPester, basicPester);
        val existingScheduler = scheduledTasks.get(pesterId);
        if (existingScheduler != null) {
            LOG.warn("Cannot schedule {} - already scheduled.", pesterId);
            return;
        }
        val taskData = new TaskData(deployedPester.getId(), basicPester.getId());
        final Runnable runnable = () -> {
            pesterDelivery.deliverPester(taskData);
        };
        final ScheduledFuture task;
        if (period instanceof Duration) {
            task = taskScheduler.scheduleAtFixedRate(runnable, then.toInstant(), (Duration) period);
        } else {
            val next = then.plus(period);
            final long ms = (next.toEpochSecond() - then.toEpochSecond()) * 1000L;
            task = taskScheduler.scheduleAtFixedRate(runnable, Date.from(then.toInstant()), ms);
        }
        scheduledTasks.put(pesterId, task);
    }

    public void removeSchedulerFor(final DeployedPesterDefinition pesterDefinition) {
        LOG.info("unscheduling {}", pesterDefinition);
        val schedulerName = pesterDefinition.getScheduler();
        val scheduler = checkScheduler(schedulerName).orElse(null);
        if (Objects.isNull(scheduler)) {
            LOG.warn("Cannot remove tasks - no such scheduler as {}", schedulerName);
            return;
        }
        for (val each : scheduler.getSchedulers()) {
            removeSchedulerInternal(pesterDefinition, each);
        }
    }

    private void removeSchedulerInternal(
            final DeployedPesterDefinition deployedPester,
            final BasicSchedulerDefinition basicPester
    ) {
        val taskId = generateTaskId(deployedPester, basicPester);
        val scheduled = scheduledTasks.get(taskId);
        if (Objects.isNull(scheduled)) {
            LOG.warn("Cannot remove task - no such task as {}", taskId);
            return;
        }
        scheduled.cancel(true);
        scheduledTasks.remove(taskId);
    }
}