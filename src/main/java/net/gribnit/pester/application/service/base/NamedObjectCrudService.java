package net.gribnit.pester.application.service.base;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.gribnit.pester.exception.CreationConflictException;
import net.gribnit.pester.exception.ModificationConflictException;
import net.gribnit.pester.exception.NotFoundException;
import net.zethmayr.benjamin.spring.common.mapper.base.Mapper;
import net.zethmayr.benjamin.spring.common.repository.base.Repository;
import net.zethmayr.benjamin.spring.common.repository.base.RepositoryException;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Function;

import static org.springframework.transaction.annotation.Isolation.REPEATABLE_READ;
import static org.springframework.transaction.annotation.Propagation.REQUIRED;

@Slf4j
public class NamedObjectCrudService<T, X> {

    private final Repository<T, Integer> baseRepository;
    private final String nameFieldName;
    private final Function<T,X> nameGetter;
    private final Mapper<T, X, X> nameMapper;
    private final String selectByName;

    public NamedObjectCrudService(Repository<T, Integer> baseRepository, String nameFieldName, Function<T,X> nameGetter) {
        this.baseRepository = baseRepository;
        this.nameFieldName = nameFieldName;
        this.nameGetter = nameGetter;
        nameMapper = baseRepository.findMapper(nameFieldName);
        if (nameMapper == null) {
            throw RepositoryException.badSetup("No mapper for field " + nameFieldName);
        }
        selectByName = baseRepository.select() + nameMapper.whereClause();
        LOG.debug("Initialized with repository {}, mapper {}", baseRepository, nameMapper);
    }

    public List<T> getAll() {
        return baseRepository.getAll();
    }

    protected List<T> getByName(final String name) {
        return baseRepository.getUnsafe(selectByName, name);
    }

    public T getOne(final String name) throws NotFoundException {
        val found = getByName(name);
        if (found.size() == 1) {
            return found.get(0);
        }
        throw NotFoundException.missing(nameFieldName + "=" + name);
    }

    @Transactional(propagation = REQUIRED, isolation = REPEATABLE_READ, rollbackFor = Throwable.class)
    public T create(String name, T object) throws CreationConflictException {
        if (!nameGetter.apply(object).equals(name)) {
            throw CreationConflictException.because("Object/name mismatch");
        }
        val existing = getByName(name);
        if (existing.size() > 0) {
            throw CreationConflictException.because("Name already exists");
        }
        try {
            baseRepository.insert(object);
        } catch (RepositoryException e) {
            LOG.error("Failed to insert: " + e.getMessage(), e);
            throw CreationConflictException.because("Cannot insert at all");
        }
        val reread = getByName(name);
        if (reread.size() != 1) {
            throw CreationConflictException.because("A duplication or write failure occurred");
        }
        created(object);
        return object;
    }

    protected void created(final T object) {
        LOG.debug("default logging creation of {}", object);
    }

    public T modify(String name, T user) throws ModificationConflictException, NotFoundException {
        val existing = getOne(name);
        return null;
    }

    public void delete(final String name) throws NotFoundException {
        val existing = getOne(name);
        deleted(existing);
        baseRepository.deleteMonadic(existing);
    }

    protected void deleted(final T object) {
        LOG.debug("default logging deletion of {}", object);
    }
}
