package net.gribnit.pester.application.service;

import lombok.extern.slf4j.Slf4j;
import net.gribnit.pester.domain.definitions.SchedulerDefinition;
import net.gribnit.pester.persistence.repositories.SchedulerRepository;
import net.gribnit.pester.application.service.base.NamedObjectCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static net.gribnit.pester.persistence.PersistenceNames.SCHEDULER_NAME;

@Service
@Slf4j
public class PesterSchedulersCrudService extends NamedObjectCrudService<SchedulerDefinition, String> {

    private final PesterEvents pesterEvents;

    public PesterSchedulersCrudService(
            final @Autowired SchedulerRepository persistedSchedulers,
            final @Autowired PesterEvents pesterEvents
    ) {
        super(persistedSchedulers, SCHEDULER_NAME, SchedulerDefinition::getName);
        this.pesterEvents = pesterEvents;
    }

    @Override
    protected void deleted(final SchedulerDefinition toDelete) {
        pesterEvents.schedulerDeleted(toDelete);
    }
}
