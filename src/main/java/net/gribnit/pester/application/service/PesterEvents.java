package net.gribnit.pester.application.service;

import lombok.val;
import net.gribnit.pester.domain.definitions.DeployedPesterDefinition;
import net.gribnit.pester.domain.definitions.GeneratorDefinition;
import net.gribnit.pester.domain.definitions.SchedulerDefinition;
import net.gribnit.pester.domain.definitions.UserDefinition;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@Service
public class PesterEvents {
    private final List<Consumer<DeployedPesterDefinition>> pesterCreatedCallbacks = new ArrayList<>();
    private final List<Consumer<DeployedPesterDefinition>> pesterDeletedCallbacks = new ArrayList<>();
    private final List<Consumer<UserDefinition>> userDeletedCallbacks = new ArrayList<>();
    private final List<Consumer<SchedulerDefinition>> schedulerDeletedCallbacks = new ArrayList<>();
    private final List<Consumer<GeneratorDefinition>> generatorDeletedCallbacks = new ArrayList<>();

    public void pesterCreated(final DeployedPesterDefinition pesterDefinition) {
        for (val each : pesterCreatedCallbacks) {
            each.accept(pesterDefinition);
        }
    }

    public void pesterDeleted(final DeployedPesterDefinition pesterDefinition) {
        for (val each : pesterDeletedCallbacks) {
            each.accept(pesterDefinition);
        }
    }

    public void userDeleted(final UserDefinition userDefinition) {
        for (val each : userDeletedCallbacks) {
            each.accept(userDefinition);
        }
    }

    public void schedulerDeleted(final SchedulerDefinition schedulerDefinition) {
        for (val each : schedulerDeletedCallbacks) {
            each.accept(schedulerDefinition);
        }
    }

    public void generatorDeleted(final GeneratorDefinition generatorDefinition) {
        for (val each : generatorDeletedCallbacks) {
            each.accept(generatorDefinition);
        }
    }

    public void listenPesterCreated(final Consumer<DeployedPesterDefinition> pesterCreated) {
        pesterCreatedCallbacks.add(pesterCreated);
    }

    public void listenPesterDeleted(final Consumer<DeployedPesterDefinition> pesterDeleted) {
        pesterDeletedCallbacks.add(pesterDeleted);
    }

    public void listenUserDeleted(final Consumer<UserDefinition> userDeleted) {
        userDeletedCallbacks.add(userDeleted);
    }

    public void listenSchedulerDeleted(final Consumer<SchedulerDefinition> schedulerDeleted) {
        schedulerDeletedCallbacks.add(schedulerDeleted);
    }

    public void listenGeneratorDeleted(final Consumer<GeneratorDefinition> generatorDeleted) {
        generatorDeletedCallbacks.add(generatorDeleted);
    }
}
