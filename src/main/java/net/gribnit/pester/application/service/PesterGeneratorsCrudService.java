package net.gribnit.pester.application.service;

import net.gribnit.pester.domain.definitions.GeneratorDefinition;
import net.gribnit.pester.persistence.repositories.GeneratorRepository;
import net.gribnit.pester.application.service.base.NamedObjectCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static net.gribnit.pester.persistence.PersistenceNames.GENERATOR_NAME;

@Service
public class PesterGeneratorsCrudService extends NamedObjectCrudService<GeneratorDefinition, String> {
    private final PesterEvents events;

    public PesterGeneratorsCrudService(
            final @Autowired GeneratorRepository baseRepository,
            final @Autowired PesterEvents events
    ) {
        super(baseRepository, GENERATOR_NAME, GeneratorDefinition::getName);
        this.events = events;
    }

    @Override
    protected void deleted(final GeneratorDefinition toDelete) {
        events.generatorDeleted(toDelete);
    }
}
