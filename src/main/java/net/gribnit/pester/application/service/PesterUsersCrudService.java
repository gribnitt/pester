package net.gribnit.pester.application.service;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.gribnit.pester.domain.definitions.UserDefinition;
import net.gribnit.pester.exception.NotFoundException;
import net.gribnit.pester.persistence.repositories.UserRepository;
import net.gribnit.pester.application.service.base.NamedObjectCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static net.gribnit.pester.persistence.PersistenceNames.USER_NAME;

@Service
@Slf4j
public class PesterUsersCrudService extends NamedObjectCrudService<UserDefinition, String> {

    private final PesterEvents pesterEvents;

    public PesterUsersCrudService(final @Autowired UserRepository persistedUsers, final @Autowired PesterEvents pesterEvents) {
        super(persistedUsers, USER_NAME, UserDefinition::getName);
        this.pesterEvents = pesterEvents;
    }

    @Deprecated
    public UserDefinition getUser(final String userName) throws NotFoundException {
        val found = getByName(userName);
        if (found.size() == 1) {
            return found.get(0);
        }
        throw NotFoundException.missing("User named " + userName);
    }

    @Override
    protected void deleted(final UserDefinition user) {
        pesterEvents.userDeleted(user);
    }
}
