package net.gribnit.pester.application.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PesterDeliveryService {
    public void deliverPester(final PesterSchedulingService.TaskData taskData) {
        LOG.warn("Not done yet... {}", taskData);
    }
}
