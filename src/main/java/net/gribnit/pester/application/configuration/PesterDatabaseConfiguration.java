package net.gribnit.pester.application.configuration;

import lombok.Data;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;

@Configuration
@ConfigurationProperties("ds")
@Data
@Accessors(chain = true)
@Slf4j
public class PesterDatabaseConfiguration {
    private String jdbcUrl;
    private String userName;
    private String password;

    @Bean
    public DataSource pesterDs() {
        final DriverManagerDataSource ds = new DriverManagerDataSource();
        ds.setDriverClassName("org.h2.Driver");
        ds.setUrl(getJdbcUrl());
        LOG.info("Using jdbc url {}", getJdbcUrl());
        ds.setUsername(getUserName());
        ds.setPassword(getPassword());
        return ds;
    }

    @Bean
    public PlatformTransactionManager transactionManager(@Autowired DataSource pesterDs) {
        return new DataSourceTransactionManager(pesterDs);
    }

    @Bean
    public JdbcTemplate pesterJdbc(@Autowired DataSource pesterDs) {
        return new JdbcTemplate(pesterDs);
    }
}
