/**
 * A REST API, scheduler, generator, and extensible delivery system for reminder messages.
 * See <a href="https://what.thedailywtf.com/topic/26040/re-go-stand-behind-him">thread</a>.
 */
package net.gribnit.pester;