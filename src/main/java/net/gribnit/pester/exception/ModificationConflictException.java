package net.gribnit.pester.exception;

public class ModificationConflictException extends ServiceException {
    public static final String MESSAGE_FORMAT = "Conflict. The resource cannot be modified to the given state: %s. You may need to delete and recreate it.";

    protected ModificationConflictException(String message) {
        super(message);
    }

    public static ModificationConflictException because(final String message) {
        return new ModificationConflictException(String.format(MESSAGE_FORMAT, message));
    }
}
