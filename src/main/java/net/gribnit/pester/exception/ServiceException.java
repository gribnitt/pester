package net.gribnit.pester.exception;

/**
 * Pokemon class for service-layer exceptions
 */
public abstract class ServiceException extends Exception {
    protected ServiceException(final String message) {
        super(message);
    }

    protected ServiceException(final Throwable cause) {
        super(cause);
    }

    protected ServiceException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
