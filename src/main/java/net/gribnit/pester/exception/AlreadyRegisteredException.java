package net.gribnit.pester.exception;

public class AlreadyRegisteredException extends RuntimeException {
    private AlreadyRegisteredException(final String message) {
        super(message);
    }

    public static <T> AlreadyRegisteredException collidesWith(final T existing) {
        return new AlreadyRegisteredException(existing + " is already registered.");
    }
}
