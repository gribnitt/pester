package net.gribnit.pester.exception;

public class NotFoundException extends ServiceException {
    protected NotFoundException(String message) {
        super(message);
    }

    public static NotFoundException missing(final Object what) {
        return new NotFoundException("Expected, but not found: " + what);
    }
}
