package net.gribnit.pester.exception;

public class SendFailedException extends ServiceException {
    private SendFailedException(final Throwable cause) {
        super(cause);
    }

    public static SendFailedException because(final Throwable cause) {
        return new SendFailedException(cause);
    }
}
