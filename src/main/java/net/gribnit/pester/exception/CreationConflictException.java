package net.gribnit.pester.exception;

public class CreationConflictException extends ServiceException {
    public static final String MESSAGE_FORMAT = "Conflict. The resource cannot be created in the given state: %s. Check your specification.";

    protected CreationConflictException(String message) {
        super(message);
    }

    public static CreationConflictException because(final String message) {
        return new CreationConflictException(String.format(MESSAGE_FORMAT, message));
    }
}
