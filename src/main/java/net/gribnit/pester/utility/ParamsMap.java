package net.gribnit.pester.utility;

import lombok.ToString;
import lombok.val;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@ToString(callSuper = true)
public class ParamsMap extends LinkedHashMap<String, Object> {
    private final Map<String, Parameter> asParameters = new HashMap<>();

    public final List<Parameter> asParameters() {
        val list = new ArrayList<Parameter>();
        for (val eachEntry : entrySet()) {
            list.add(getOrCreateParameter(eachEntry));
        }
        return list;
    }

    private Parameter getOrCreateParameter(final Map.Entry<String, Object> outerEntry) {
        return asParameters.computeIfAbsent(
                outerEntry.getKey(),
                (name) -> {
                    val p = new Parameter();
                    p.setKey(name);
                    p.setValue(outerEntry.getValue());
                    return p;
                }
        );
    }

    public void putParameter(final Parameter parameter) {
        put(parameter.getKey(), parameter.getValue());
        asParameters.put(parameter.getKey(), parameter);
    }
}
