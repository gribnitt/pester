package net.gribnit.pester.utility;

import lombok.val;
import net.zethmayr.benjamin.spring.common.util.MapBuilder;

import java.util.function.Consumer;

public class ParamsBuilder {

    public static MapBuilder<String,Object, ParamsMap> paramsBuilder() {
        return MapBuilder.on(new ParamsMap());
    }

    @SafeVarargs
    public static void onParams(final ParamsMap params, final Consumer<MapBuilder<String, Object, ParamsMap>>... paramMutators) {
        val paramsBuilder = MapBuilder.on(params);
        for (val mutator : paramMutators) {
            mutator.accept(paramsBuilder);
        }
    }
}
