package net.gribnit.pester.utility;

import lombok.Data;
import lombok.val;

import java.util.Map;

/**
 * A parameter map entry
 */
@Data
public class Parameter implements Map.Entry<String, Object> {
    private Integer id;
    private Integer ownerId;
    private String key;
    private Object value;

    @Override
    public Object setValue(final Object newValue) {
        val toReturn = value;
        value = newValue;
        return toReturn;
    }
}
