package net.gribnit.pester.utility;

import java.util.function.Consumer;
import java.util.function.Function;

public class Danger {

    @FunctionalInterface
    public interface ThrowingFunction<T, R, E extends Exception> {
        R apply(final T input) throws E;
    }

    public static <T, R, E extends Exception> Function<T, R> unchecked(
            final ThrowingFunction<T, R, E> checked
    ) {
        return (t) -> {
            final R result;
            try {
                result = checked.apply(t);
            } catch (Exception e) {
                if (e instanceof RuntimeException) {
                    throw (RuntimeException)e;
                }
                throw new RuntimeException(e);
            }
            return result;
        };
    }
}
