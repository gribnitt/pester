package net.gribnit.pester.utility;

import net.zethmayr.benjamin.spring.common.mapper.base.ColumnType;
import net.zethmayr.benjamin.spring.common.mapper.base.ComposedMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.InvertibleRowMapperBase;
import net.zethmayr.benjamin.spring.common.mapper.base.Mapper;

import java.util.Arrays;

public class RelatedStringMapper extends InvertibleRowMapperBase<RelatedString> {
    public static final Mapper<RelatedString, Integer, Integer> VALUE_ROW_ID = ComposedMapper.simpleField(
            "id",
            RelatedString::getId,
            ColumnType.INTEGER_INDEX,
            RelatedString::setId
    );

    public static final Mapper<RelatedString, Integer, Integer> OWNER_ROW_ID = ComposedMapper.simpleField(
            "owner_id",
            RelatedString::getOwnerId,
            ColumnType.INTEGER,
            RelatedString::setOwnerId
    );

    public static final Mapper<RelatedString, String, String> THE_STRING = ComposedMapper.simpleField(
            "the_string",
            RelatedString::getValue,
            ColumnType.LONG_STRING,
            RelatedString::setValue
    );

    public RelatedStringMapper(final String table) {
        super(RelatedString.class, Arrays.asList(VALUE_ROW_ID, OWNER_ROW_ID, THE_STRING), table, RelatedString::new);
    }
}
