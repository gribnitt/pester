/**
 * Dependency-free utility classes and methods.
 *
 * This package is not allowed to have any dependencies in {@link net.gribnit.pester}.
 * Any package may depend on this package, whatever its package-info says.
 */
package net.gribnit.pester.utility;