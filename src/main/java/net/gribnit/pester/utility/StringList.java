package net.gribnit.pester.utility;

import lombok.ToString;
import lombok.val;
import net.zethmayr.benjamin.spring.common.util.ListBuilder;

import java.util.ArrayList;
import java.util.List;

@ToString(callSuper = true)
public class StringList extends ArrayList<String> {
    private final ArrayList<RelatedString> relatedStrings = new ArrayList<>();

    public static ListBuilder<String, StringList> builder() {
        return ListBuilder.on(new StringList());
    }

    public List<RelatedString> asRelatedStrings() {
        val size = size();
        relatedStrings.ensureCapacity(size);
        val reSize = relatedStrings.size();
        val toReturn = new ArrayList<RelatedString>(size);
        for (int i = 0; i < size; i++) {
            toReturn.add(i, getOrCreateRelatedString(i, null));
        }
        return toReturn;
    }

    private RelatedString getOrCreateRelatedString(final int i, final RelatedString presented) {
        val got = get(i);
        val reSize = relatedStrings.size();
        val existing = i < reSize ? relatedStrings.get(i) : null;
        if (existing == null) {
            val created = new RelatedString().setValue(got).from(presented);
            if (i < reSize) {
                relatedStrings.set(i, created);
            } else {
                relatedStrings.add(i, created);
            }
            return created;
        } else {
            existing.setValue(got).from(presented);
        }
        return existing;
    }

    public void addRelatedString(final RelatedString relatedString) {
        add(relatedString.getValue());
        getOrCreateRelatedString(size() - 1, relatedString);
    }
}
