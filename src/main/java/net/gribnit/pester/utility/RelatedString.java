package net.gribnit.pester.utility;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class RelatedString {
    private int id;
    private int ownerId;
    private String value;

    public RelatedString from(RelatedString presented) {
        if (presented != null) {
            this.id = presented.id;
            this.ownerId = presented.ownerId;
            this.value = presented.value;
        }
        return this;
    }
}
