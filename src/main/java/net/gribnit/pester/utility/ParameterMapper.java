package net.gribnit.pester.utility;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.ClassFieldMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.ColumnType;
import net.zethmayr.benjamin.spring.common.mapper.base.ComposedMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.FieldMapperTransform;
import net.zethmayr.benjamin.spring.common.mapper.base.InvertibleRowMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.InvertibleRowMapperBase;
import net.zethmayr.benjamin.spring.common.mapper.base.Mapper;
import net.zethmayr.benjamin.spring.common.mapper.base.RowMapperTransform;

import java.sql.ResultSet;
import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

import static net.gribnit.pester.utility.Danger.unchecked;

public class ParameterMapper implements InvertibleRowMapper<Parameter> {
    final ObjectMapper om;

    public static final Mapper<Parameter, Integer, Integer> PARAM_ROW_ID = ComposedMapper.simpleField(
            "id",
            Parameter::getId,
            ColumnType.INTEGER_INDEX,
            Parameter::setId
    );

    public static final Mapper<Parameter, Integer, Integer> OWNER_ROW_ID = ComposedMapper.simpleField(
            "owner_id",
            Parameter::getOwnerId,
            ColumnType.INTEGER,
            Parameter::setOwnerId
    );

    public static final Mapper<Parameter, String, String> PARAM_NAME = ComposedMapper.simpleField(
            "param_name",
            Parameter::getKey,
            ColumnType.LONG_STRING,
            Parameter::setKey
    );

    public final Mapper<Parameter, Object, String> Param_Value;

    /**
     * Note - after cloning this _is_ getting called directly, the delegates are gone. But we got to initialize it a skosh more dynamically.
     */
    private final InvertibleRowMapperBase<Parameter> delegate;

    public ParameterMapper(final String table, final ObjectMapper om) {
        this.om = om == null ? new ObjectMapper() : om;
        this.Param_Value = ComposedMapper.<Parameter, Object, String>field(
                "param_value",
                Parameter::getValue,
                (o) -> unchecked(om::writeValueAsString).apply(o),
                ColumnType.LONG_STRING,
                (s) -> unchecked((ss) -> om.readValue((String)ss, Object.class)).apply(s),
                Parameter::setValue
        );

        delegate = new InvertibleRowMapperBase<Parameter>(Parameter.class, Arrays.asList(PARAM_ROW_ID, OWNER_ROW_ID, PARAM_NAME, Param_Value), table, Parameter::new) {
        };
    }

    // we allow the clone to just happen as normal - so these are not reliably going to get called, ever

    @Override
    public Class<Parameter> rowClass() {
        return delegate.rowClass();
    }

    @Override
    public List<ClassFieldMapper<Parameter>> fields() {
        return delegate.fields();
    }

    @Override
    public List<ClassFieldMapper<Parameter>> mappableFields() {
        return delegate.mappableFields();
    }

    @Override
    public String table() {
        return delegate.table();
    }

    @Override
    public String select() {
        return delegate.select();
    }

    @Override
    public String insert() {
        return delegate.insert();
    }

    @Override
    public Supplier<Parameter> empty() {
        return delegate.empty();
    }

    @Override
    public Parameter mapRow(ResultSet rs, int i) {
        return delegate.mapRow(rs,i);
    }

    @Override
    public Object[] getInsertValues(Parameter insert) {
        return delegate.getInsertValues(insert);
    }

    @Override
    public InvertibleRowMapper<Parameter> copyTransforming(RowMapperTransform mapperTransform, FieldMapperTransform fieldTransform) {
        return delegate.copyTransforming(mapperTransform, fieldTransform);
    }

    @Override
    public Mapper<Parameter, ?, ?> idMapper() {
        return delegate.idMapper();
    }
}
