package net.gribnit.pester;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import lombok.val;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.Clock;

@SpringBootApplication
@EnableTransactionManagement(mode = AdviceMode.ASPECTJ, proxyTargetClass = true)
@EnableSwagger2
@ComponentScan({
        "net.gribnit.pester",
        "net.zethmayr.benjamin.spring.common"
})
@SuppressWarnings("unused") // they're IOC entry points
public class PesterApplication {
    public static void main(final String... args) {
        final SpringApplication app = new SpringApplication(PesterApplication.class);
        app.run(args);
    }

    /**
     * We're using Jackson for JSON serialization / deserialization
     *
     * @return The {@link ObjectMapper}
     */
    @Bean
    public ObjectMapper om() {
        val om = new ObjectMapper();
        om.enable(SerializationFeature.INDENT_OUTPUT);
        return om;
    }

    /**
     * We're using Apache HTTPClient because it supports PATCH
     *
     * @return The {@link HttpClient}
     */
    @Bean
    public CloseableHttpClient httpClient() {
        return HttpClients.custom()
                .build();
    }

    @Bean
    public Clock theClock() {
        return Clock.systemDefaultZone();
    }

    @Bean
    public ThreadPoolTaskScheduler taskScheduler() {
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        taskScheduler.setPoolSize(3);
        taskScheduler.setThreadNamePrefix("scheduled");
        return taskScheduler;
    }

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("net.gribnit.pester"))
                .paths(PathSelectors.any())
                .build();
    }
}
