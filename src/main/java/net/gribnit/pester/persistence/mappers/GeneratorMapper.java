package net.gribnit.pester.persistence.mappers;

import net.gribnit.pester.domain.definitions.GeneratorDefinition;
import net.gribnit.pester.utility.RelatedString;
import net.gribnit.pester.utility.RelatedStringMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.ColumnType;
import net.zethmayr.benjamin.spring.common.mapper.base.ComposedMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.InvertibleRowMapperBase;
import net.zethmayr.benjamin.spring.common.mapper.base.JoiningRowMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.Mapper;
import net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin;

import java.util.Arrays;

import static net.gribnit.pester.persistence.PersistenceNames.GENERATOR_NAME;
import static net.gribnit.pester.persistence.PersistenceNames.GENERATOR_TEMPLATES;
import static net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin.DeleteStyle.USE_PARENT_ID;
import static net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin.InsertStyle.NEEDS_PARENT_ID;
import static net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin.collection;
import static net.zethmayr.benjamin.spring.common.mapper.base.SqlOp.EQ;

public class GeneratorMapper extends JoiningRowMapper<GeneratorDefinition> {
    public static final Mapper<GeneratorDefinition, Integer, Integer> ID = ComposedMapper.simpleField(
            "id",
            GeneratorDefinition::getId,
            ColumnType.INTEGER_INDEX,
            GeneratorDefinition::setId
    );

    public static final Mapper<GeneratorDefinition, String, String> NAME = ComposedMapper.simpleField(
            GENERATOR_NAME,
            GeneratorDefinition::getName,
            ColumnType.LONG_STRING,
            GeneratorDefinition::setName
    );

    public static class CoreMapper extends InvertibleRowMapperBase<GeneratorDefinition> {
        public CoreMapper() {
            super(GeneratorDefinition.class, Arrays.asList(ID, NAME), "generators", GeneratorDefinition::new);
        }
    }

    public GeneratorMapper() {
        super(new CoreMapper(),
                MapperAndJoin.<GeneratorDefinition, RelatedString, Integer>builder()
                        .parentField(ID)
                        .relation(EQ)
                        .relatedField(RelatedStringMapper.OWNER_ROW_ID)
                        .getter(collection(g -> g.getTemplates().asRelatedStrings()))
                        .acceptor((g, r) -> g.getTemplates().addRelatedString(r))
                        .mapper(new RelatedStringMapper(GENERATOR_TEMPLATES))
                        .insertions(NEEDS_PARENT_ID)
                        .deletions(USE_PARENT_ID)
                        .build()
        );
    }
}
