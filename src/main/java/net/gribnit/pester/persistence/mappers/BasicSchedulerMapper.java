package net.gribnit.pester.persistence.mappers;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.gribnit.pester.domain.definitions.BasicSchedulerDefinition;
import net.gribnit.pester.utility.Parameter;
import net.gribnit.pester.utility.ParameterMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.ColumnType;
import net.zethmayr.benjamin.spring.common.mapper.base.ComposedMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.InvertibleRowMapperBase;
import net.zethmayr.benjamin.spring.common.mapper.base.JoiningRowMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.Mapper;
import net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin;

import java.util.Arrays;

import static net.gribnit.pester.persistence.PersistenceNames.BASIC_SCHEDULER_PARAMS;
import static net.gribnit.pester.persistence.PersistenceNames.GENERATOR_NAME;
import static net.gribnit.pester.persistence.PersistenceNames.SCHEDULER_TYPE;
import static net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin.DeleteStyle.USE_PARENT_ID;
import static net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin.InsertStyle.NEEDS_PARENT_ID;
import static net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin.collection;
import static net.zethmayr.benjamin.spring.common.mapper.base.SqlOp.EQ;

public class BasicSchedulerMapper extends JoiningRowMapper<BasicSchedulerDefinition> {
    public static final Mapper<BasicSchedulerDefinition, Integer, Integer> ID = ComposedMapper.simpleField(
            "id",
            BasicSchedulerDefinition::getId,
            ColumnType.INTEGER_INDEX,
            BasicSchedulerDefinition::setId
    );

    public static final Mapper<BasicSchedulerDefinition, Integer, Integer> SCHEDULER_ID = ComposedMapper.simpleField(
            "scheduler_id",
            BasicSchedulerDefinition::getSchedulerId,
            ColumnType.INTEGER,
            BasicSchedulerDefinition::setSchedulerId
    );

    public static final Mapper<BasicSchedulerDefinition, String, String> TYPE = ComposedMapper.simpleField(
            SCHEDULER_TYPE,
            BasicSchedulerDefinition::getType,
            ColumnType.LONG_STRING,
            BasicSchedulerDefinition::setType
    );

    public static final Mapper<BasicSchedulerDefinition, String, String> GENERATOR = ComposedMapper.simpleField(
            GENERATOR_NAME,
            BasicSchedulerDefinition::getGenerator,
            ColumnType.LONG_STRING,
            BasicSchedulerDefinition::setGenerator
    );

    public static class CoreMapper extends InvertibleRowMapperBase<BasicSchedulerDefinition> {
        public CoreMapper() {
            super(BasicSchedulerDefinition.class, Arrays.asList(
                    ID,
                    SCHEDULER_ID,
                    TYPE,
                    GENERATOR
            ), "basic_schedulers", BasicSchedulerDefinition::new);
        }
    }

    public BasicSchedulerMapper(final ObjectMapper om) {
        super(new CoreMapper(),
                MapperAndJoin.<BasicSchedulerDefinition, Parameter, Integer>builder()
                        .parentField(ID)
                        .relation(EQ)
                        .relatedField(ParameterMapper.OWNER_ROW_ID)
                        .insertions(NEEDS_PARENT_ID)
                        .deletions(USE_PARENT_ID)
                        .mapper(new ParameterMapper(BASIC_SCHEDULER_PARAMS, om))
                        .getter(collection((d) -> d.getParams().asParameters()))
                        .acceptor((d, p) -> d.getParams().putParameter(p))
                        .build()
        );
    }
}
