package net.gribnit.pester.persistence.mappers;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.gribnit.pester.domain.definitions.DeployedPesterDefinition;
import net.gribnit.pester.persistence.PersistenceNames;
import net.gribnit.pester.utility.Parameter;
import net.gribnit.pester.utility.ParameterMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.ClassFieldMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.ColumnType;
import net.zethmayr.benjamin.spring.common.mapper.base.ComposedMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.InvertibleRowMapperBase;
import net.zethmayr.benjamin.spring.common.mapper.base.JoiningRowMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.Mapper;
import net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import static net.gribnit.pester.persistence.PersistenceNames.DEPLOYED_PESTER_PARAMS;
import static net.gribnit.pester.persistence.PersistenceNames.GENERATOR_NAME;
import static net.gribnit.pester.persistence.PersistenceNames.SCHEDULER_NAME;
import static net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin.DeleteStyle.USE_PARENT_ID;
import static net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin.InsertStyle.NEEDS_PARENT_ID;
import static net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin.collection;
import static net.zethmayr.benjamin.spring.common.mapper.base.SqlOp.EQ;

public class DeployedPesterMapper extends JoiningRowMapper<DeployedPesterDefinition> {

    public static final Mapper<DeployedPesterDefinition, Integer, Integer> ID = ComposedMapper.simpleField(
            "id",
            DeployedPesterDefinition::getId,
            ColumnType.INTEGER_INDEX,
            DeployedPesterDefinition::setId
    );

    public static final Mapper<DeployedPesterDefinition, Integer, Integer> USER_ID = ComposedMapper.simpleField(
            PersistenceNames.USER_ID,
            DeployedPesterDefinition::getUserId,
            ColumnType.INTEGER,
            DeployedPesterDefinition::setUserId
    );

    public static final Mapper<DeployedPesterDefinition, String, String> SCHEDULER = ComposedMapper.simpleField(
            SCHEDULER_NAME,
            DeployedPesterDefinition::getScheduler,
            ColumnType.LONG_STRING,
            DeployedPesterDefinition::setScheduler
    );

    public static final Mapper<DeployedPesterDefinition, String, String> GENERATOR = ComposedMapper.simpleField(
            GENERATOR_NAME,
            DeployedPesterDefinition::getGenerator,
            ColumnType.LONG_STRING,
            DeployedPesterDefinition::setGenerator
    );

    public static final Mapper<DeployedPesterDefinition, Instant, Instant> DEPLOYED_AT = ComposedMapper.simpleField(
            "deployed_at",
            DeployedPesterDefinition::getDeployed,
            ColumnType.INSTANT,
            DeployedPesterDefinition::setDeployed
    );

    public static final List<ClassFieldMapper<DeployedPesterDefinition>> CORE_FIELDS = Arrays.asList(
        ID, USER_ID, SCHEDULER, GENERATOR, DEPLOYED_AT
    );

    public static final String TABLE = "deployed_pesters";

    public static class CoreMapper extends InvertibleRowMapperBase<DeployedPesterDefinition> {
        public CoreMapper() {
            super(DeployedPesterDefinition.class, CORE_FIELDS, TABLE, DeployedPesterDefinition::new);
        }
    }

    public DeployedPesterMapper(final ObjectMapper om) {
        super(new CoreMapper(),
                MapperAndJoin.<DeployedPesterDefinition, Parameter, Integer>builder()
                        .parentField(ID)
                        .relation(EQ)
                        .relatedField(ParameterMapper.OWNER_ROW_ID)
                        .insertions(NEEDS_PARENT_ID)
                        .deletions(USE_PARENT_ID)
                        .mapper(new ParameterMapper(DEPLOYED_PESTER_PARAMS, om))
                        .getter(collection((d) -> d.getParams().asParameters()))
                        .acceptor((d,p) -> d.getParams().putParameter(p))
                        .build()
                );
    }
}
