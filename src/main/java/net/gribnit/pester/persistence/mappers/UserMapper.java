package net.gribnit.pester.persistence.mappers;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.gribnit.pester.domain.definitions.ContactMethodDefinition;
import net.gribnit.pester.domain.definitions.UserDefinition;
import net.zethmayr.benjamin.spring.common.mapper.base.ClassFieldMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.ColumnType;
import net.zethmayr.benjamin.spring.common.mapper.base.ComposedMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.InvertibleRowMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.InvertibleRowMapperBase;
import net.zethmayr.benjamin.spring.common.mapper.base.JoiningRowMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.Mapper;
import net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin;

import java.util.Arrays;
import java.util.List;

import static net.gribnit.pester.persistence.PersistenceNames.USER_NAME;
import static net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin.DeleteStyle.USE_PARENT_ID;
import static net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin.InsertStyle.NEEDS_PARENT_ID;
import static net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin.adder;
import static net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin.collection;
import static net.zethmayr.benjamin.spring.common.mapper.base.SqlOp.EQ;

public class UserMapper extends JoiningRowMapper<UserDefinition> {

    public static final Mapper<UserDefinition, Integer, Integer> ID = ComposedMapper.simpleField(
            "id",
            UserDefinition::getId,
            ColumnType.INTEGER_INDEX,
            UserDefinition::setId
    );

    public static final Mapper<UserDefinition, String, String> NAME = ComposedMapper.simpleField(
            USER_NAME,
            UserDefinition::getName,
            ColumnType.LONG_STRING,
            UserDefinition::setName
    );

    public static final List<ClassFieldMapper<UserDefinition>> CORE_FIELDS = Arrays.asList(
            ID, NAME
    );

    public static class CoreMapper extends InvertibleRowMapperBase<UserDefinition> {
        public CoreMapper() {
            super(UserDefinition.class, CORE_FIELDS, "pester_users", UserDefinition::new);
        }
    }

    public UserMapper(final ObjectMapper om) {
        super(new CoreMapper(),
                MapperAndJoin.<UserDefinition, ContactMethodDefinition, Integer>builder()
                        .parentField(ID)
                        .relation(EQ)
                        .relatedField(ContactMethodMapper.USER_ID)
                        .mapper(new ContactMethodMapper(om))
                        .acceptor(adder(UserDefinition::getContactMethods))
                        .getter(collection(UserDefinition::getContactMethods))
                        .insertions(NEEDS_PARENT_ID)
                        .deletions(USE_PARENT_ID)
                        .build()
        );
    }
}
