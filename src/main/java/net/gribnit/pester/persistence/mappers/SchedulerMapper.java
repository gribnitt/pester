package net.gribnit.pester.persistence.mappers;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.gribnit.pester.domain.definitions.BasicSchedulerDefinition;
import net.gribnit.pester.domain.definitions.SchedulerDefinition;
import net.zethmayr.benjamin.spring.common.mapper.base.ColumnType;
import net.zethmayr.benjamin.spring.common.mapper.base.ComposedMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.InvertibleRowMapperBase;
import net.zethmayr.benjamin.spring.common.mapper.base.JoiningRowMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.Mapper;
import net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin;

import java.util.Arrays;

import static net.gribnit.pester.persistence.PersistenceNames.GENERATOR_NAME;
import static net.gribnit.pester.persistence.PersistenceNames.SCHEDULER_NAME;
import static net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin.DeleteStyle.USE_PARENT_ID;
import static net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin.InsertStyle.NEEDS_PARENT_ID;
import static net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin.adder;
import static net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin.collection;
import static net.zethmayr.benjamin.spring.common.mapper.base.SqlOp.EQ;

public class SchedulerMapper extends JoiningRowMapper<SchedulerDefinition> {
    public static final Mapper<SchedulerDefinition, Integer, Integer> ID = ComposedMapper.simpleField(
            "id",
            SchedulerDefinition::getId,
            ColumnType.INTEGER_INDEX,
            SchedulerDefinition::setId
    );

    public static final Mapper<SchedulerDefinition, String, String> NAME = ComposedMapper.simpleField(
            SCHEDULER_NAME,
            SchedulerDefinition::getName,
            ColumnType.LONG_STRING,
            SchedulerDefinition::setName
    );

    public static final Mapper<SchedulerDefinition, String, String> GENERATOR = ComposedMapper.simpleField(
            GENERATOR_NAME,
            SchedulerDefinition::getGenerator,
            ColumnType.LONG_STRING,
            SchedulerDefinition::setGenerator
    );

    public static class CoreMapper extends InvertibleRowMapperBase<SchedulerDefinition> {
        public CoreMapper() {
            super(SchedulerDefinition.class, Arrays.asList(
                    ID, NAME, GENERATOR
            ), "schedulers", SchedulerDefinition::new);
        }
    }

    public SchedulerMapper(final ObjectMapper om) {
        super(new CoreMapper(), MapperAndJoin.<SchedulerDefinition, BasicSchedulerDefinition, Integer>builder()
                .parentField(ID)
                .relation(EQ)
                .relatedField(BasicSchedulerMapper.SCHEDULER_ID)
                .mapper(new BasicSchedulerMapper(om))
                .getter(collection(SchedulerDefinition::getSchedulers))
                .acceptor(adder(SchedulerDefinition::getSchedulers))
                .insertions(NEEDS_PARENT_ID)
                .deletions(USE_PARENT_ID)
                .build());
    }
}
