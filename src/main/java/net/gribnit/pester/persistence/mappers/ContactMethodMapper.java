package net.gribnit.pester.persistence.mappers;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.gribnit.pester.domain.definitions.ContactMethodDefinition;
import net.gribnit.pester.utility.Parameter;
import net.gribnit.pester.utility.ParameterMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.ClassFieldMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.ColumnType;
import net.zethmayr.benjamin.spring.common.mapper.base.ComposedMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.InvertibleRowMapperBase;
import net.zethmayr.benjamin.spring.common.mapper.base.JoiningRowMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.Mapper;
import net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin;

import java.util.Arrays;
import java.util.List;

import static net.gribnit.pester.persistence.PersistenceNames.CONTACT_METHODS;
import static net.gribnit.pester.persistence.PersistenceNames.CONTACT_METHOD_PARAMS;
import static net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin.DeleteStyle.USE_PARENT_ID;
import static net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin.InsertStyle.NEEDS_PARENT_ID;
import static net.zethmayr.benjamin.spring.common.mapper.base.MapperAndJoin.collection;
import static net.zethmayr.benjamin.spring.common.mapper.base.SqlOp.EQ;

public class ContactMethodMapper extends JoiningRowMapper<ContactMethodDefinition> {

    public static final Mapper<ContactMethodDefinition, Integer, Integer> ID = ComposedMapper.simpleField(
            "id",
            ContactMethodDefinition::getId,
            ColumnType.INTEGER_INDEX,
            ContactMethodDefinition::setId
    );

    public static final Mapper<ContactMethodDefinition, Integer, Integer> USER_ID = ComposedMapper.simpleField(
            "user_id",
            ContactMethodDefinition::getUserId,
            ColumnType.INTEGER,
            ContactMethodDefinition::setUserId
    );

    public static final Mapper<ContactMethodDefinition, String, String> NAME = ComposedMapper.simpleField(
            "method_name",
            ContactMethodDefinition::getName,
            ColumnType.LONG_STRING,
            ContactMethodDefinition::setName
    );

    public static final Mapper<ContactMethodDefinition, String, String> URL = ComposedMapper.simpleField(
            "url",
            ContactMethodDefinition::getUrl,
            ColumnType.LONG_STRING,
            ContactMethodDefinition::setUrl
    );

    public static final List<ClassFieldMapper<ContactMethodDefinition>> CORE_FIELDS = Arrays.asList(
            ID, USER_ID, NAME, URL
    );

    public static class CoreMapper extends InvertibleRowMapperBase<ContactMethodDefinition> {
        public CoreMapper() {
            super(ContactMethodDefinition.class, CORE_FIELDS, CONTACT_METHODS, ContactMethodDefinition::new);
        }
    }

    public ContactMethodMapper(final ObjectMapper om) {
        super(new CoreMapper(),
                MapperAndJoin.<ContactMethodDefinition, Parameter, Integer>builder()
                        .parentField(ID)
                        .relation(EQ)
                        .relatedField(ParameterMapper.OWNER_ROW_ID)
                        .insertions(NEEDS_PARENT_ID)
                        .deletions(USE_PARENT_ID)
                        .mapper(new ParameterMapper(CONTACT_METHOD_PARAMS, om))
                        .getter(collection((d) -> d.getParams().asParameters()))
                        .acceptor((d, p) -> d.getParams().putParameter(p))
                        .build()
        );
    }
}
