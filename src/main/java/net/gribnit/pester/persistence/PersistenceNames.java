package net.gribnit.pester.persistence;

public final class PersistenceNames {


    private PersistenceNames() {
    }

    public static final String BASIC_SCHEDULER_PARAMS = "basic_scheduler_params";
    public static final String CONTACT_METHOD_PARAMS = "contact_method_params";
    public static final String CONTACT_METHODS = "contact_methods";
    public static final String DEPLOYED_PESTER_PARAMS = "deployed_pester_params";
    public static final String GENERATOR_NAME = "generator_name";
    public static final String GENERATOR_TEMPLATES = "generator_templates";
    public static final String SCHEDULER_NAME = "scheduler_name"; // A scheduler name
    public static final String SCHEDULER_TYPE = "scheduler_type"; // A basic scheduler name
    public static final String USER_NAME = "user_name";
    public static final String USER_ID = "user_id";
    public static final String SUBJECT = "subject";
}
