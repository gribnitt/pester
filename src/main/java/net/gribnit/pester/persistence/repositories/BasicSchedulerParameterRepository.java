package net.gribnit.pester.persistence.repositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.gribnit.pester.utility.Parameter;
import net.gribnit.pester.utility.ParameterMapper;
import net.zethmayr.benjamin.spring.common.repository.base.MapperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import static net.gribnit.pester.persistence.PersistenceNames.BASIC_SCHEDULER_PARAMS;
import static net.gribnit.pester.persistence.PersistenceNames.CONTACT_METHOD_PARAMS;

@Service
public class BasicSchedulerParameterRepository extends MapperRepository<Parameter, Integer> {
    public BasicSchedulerParameterRepository(final @Autowired JdbcTemplate jdbcTemplate, final @Autowired ObjectMapper om) {
        super(jdbcTemplate, new ParameterMapper(BASIC_SCHEDULER_PARAMS, om), ParameterMapper.PARAM_ROW_ID);
    }
}
