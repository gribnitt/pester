package net.gribnit.pester.persistence.repositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.gribnit.pester.domain.definitions.BasicSchedulerDefinition;
import net.gribnit.pester.persistence.mappers.BasicSchedulerMapper;
import net.zethmayr.benjamin.spring.common.repository.base.JoiningRepository;
import net.zethmayr.benjamin.spring.common.repository.base.MapperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class BasicSchedulerRepository extends JoiningRepository<BasicSchedulerDefinition, Integer> {
    public static class CoreRepository extends MapperRepository<BasicSchedulerDefinition, Integer> {
        private CoreRepository(final JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, new BasicSchedulerMapper.CoreMapper(), BasicSchedulerMapper.ID);
        }
    }

    public BasicSchedulerRepository(final @Autowired JdbcTemplate jdbcTemplate, final @Autowired ObjectMapper om,
                                    final BasicSchedulerParameterRepository basicSchedulerParams) {
        super(jdbcTemplate, new BasicSchedulerMapper(om), new CoreRepository(jdbcTemplate), basicSchedulerParams);
    }
}
