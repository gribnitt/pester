package net.gribnit.pester.persistence.repositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.gribnit.pester.utility.ParameterMapper;
import net.gribnit.pester.utility.Parameter;
import net.zethmayr.benjamin.spring.common.repository.base.MapperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import static net.gribnit.pester.persistence.PersistenceNames.CONTACT_METHOD_PARAMS;

@Service
public class ContactMethodParameterRepository extends MapperRepository<Parameter, Integer> {
    public ContactMethodParameterRepository(final @Autowired JdbcTemplate jdbcTemplate, final @Autowired ObjectMapper om) {
        super(jdbcTemplate, new ParameterMapper(CONTACT_METHOD_PARAMS, om), ParameterMapper.PARAM_ROW_ID);
    }
}
