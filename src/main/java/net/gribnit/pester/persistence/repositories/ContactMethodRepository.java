package net.gribnit.pester.persistence.repositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.gribnit.pester.domain.definitions.ContactMethodDefinition;
import net.gribnit.pester.persistence.mappers.ContactMethodMapper;
import net.zethmayr.benjamin.spring.common.repository.base.JoiningRepository;
import net.zethmayr.benjamin.spring.common.repository.base.MapperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class ContactMethodRepository extends JoiningRepository<ContactMethodDefinition, Integer> {

    public static class CoreRepository extends MapperRepository<ContactMethodDefinition, Integer> {
        private CoreRepository(JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, new ContactMethodMapper.CoreMapper(), ContactMethodMapper.ID);
        }
    }

    public ContactMethodRepository(final @Autowired JdbcTemplate jdbcTemplate, final @Autowired ObjectMapper om, final @Autowired ContactMethodParameterRepository methodParams) {
        super(jdbcTemplate, new ContactMethodMapper(om), new CoreRepository(jdbcTemplate), methodParams);
    }
}
