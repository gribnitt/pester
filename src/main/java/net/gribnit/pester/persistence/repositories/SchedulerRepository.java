package net.gribnit.pester.persistence.repositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.gribnit.pester.domain.definitions.SchedulerDefinition;
import net.gribnit.pester.persistence.mappers.SchedulerMapper;
import net.zethmayr.benjamin.spring.common.repository.base.JoiningRepository;
import net.zethmayr.benjamin.spring.common.repository.base.MapperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class SchedulerRepository extends JoiningRepository<SchedulerDefinition, Integer> {
    public static class CoreRepository extends MapperRepository<SchedulerDefinition, Integer> {
        public CoreRepository(final JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, new SchedulerMapper.CoreMapper(), SchedulerMapper.ID);
        }
    }

    public SchedulerRepository(final @Autowired JdbcTemplate jdbcTemplate, final @Autowired ObjectMapper om,
                               final @Autowired BasicSchedulerRepository basicSchedulers, final @Autowired BasicSchedulerParameterRepository basicSchedulerParams) {
        super(jdbcTemplate, new SchedulerMapper(om), new CoreRepository(jdbcTemplate), basicSchedulers, basicSchedulerParams);
    }
}
