package net.gribnit.pester.persistence.repositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.gribnit.pester.utility.Parameter;
import net.gribnit.pester.utility.ParameterMapper;
import net.zethmayr.benjamin.spring.common.repository.base.MapperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import static net.gribnit.pester.persistence.PersistenceNames.CONTACT_METHOD_PARAMS;
import static net.gribnit.pester.persistence.PersistenceNames.DEPLOYED_PESTER_PARAMS;

@Service
public class DeployedPesterParameterRepository extends MapperRepository<Parameter, Integer> {
    public DeployedPesterParameterRepository(final @Autowired JdbcTemplate jdbcTemplate, final @Autowired ObjectMapper om) {
        super(jdbcTemplate, new ParameterMapper(DEPLOYED_PESTER_PARAMS, om), ParameterMapper.PARAM_ROW_ID);
    }
}
