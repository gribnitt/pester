package net.gribnit.pester.persistence.repositories;

import net.gribnit.pester.domain.definitions.GeneratorDefinition;
import net.gribnit.pester.persistence.mappers.GeneratorMapper;
import net.zethmayr.benjamin.spring.common.repository.base.JoiningRepository;
import net.zethmayr.benjamin.spring.common.repository.base.MapperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class GeneratorRepository extends JoiningRepository<GeneratorDefinition, Integer> {
    public static class CoreRepository extends MapperRepository<GeneratorDefinition, Integer> {
        public CoreRepository(final JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, new GeneratorMapper.CoreMapper(), GeneratorMapper.ID);
        }
    }

    public GeneratorRepository(final @Autowired JdbcTemplate jdbcTemplate, final @Autowired GeneratorTemplateRepository templates) {
        super(jdbcTemplate, new GeneratorMapper(), new CoreRepository(jdbcTemplate), templates);
    }
}
