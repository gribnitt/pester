package net.gribnit.pester.persistence.repositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.gribnit.pester.domain.definitions.DeployedPesterDefinition;
import net.gribnit.pester.persistence.mappers.DeployedPesterMapper;
import net.zethmayr.benjamin.spring.common.repository.base.JoiningRepository;
import net.zethmayr.benjamin.spring.common.repository.base.MapperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class DeployedPesterRepository extends JoiningRepository<DeployedPesterDefinition, Integer> {

    public static final class CoreRepository extends MapperRepository<DeployedPesterDefinition, Integer> {
        private CoreRepository(final JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, new DeployedPesterMapper.CoreMapper(), DeployedPesterMapper.ID);
        }
    }

    public DeployedPesterRepository(final @Autowired JdbcTemplate jdbcTemplate, final @Autowired ObjectMapper om,
                                    final @Autowired DeployedPesterParameterRepository pesterParams) {
        super(jdbcTemplate, new DeployedPesterMapper(om), new CoreRepository(jdbcTemplate), pesterParams);
    }
}
