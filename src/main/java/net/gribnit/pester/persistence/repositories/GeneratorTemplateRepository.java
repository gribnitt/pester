package net.gribnit.pester.persistence.repositories;

import net.gribnit.pester.utility.RelatedString;
import net.gribnit.pester.utility.RelatedStringMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.InvertibleRowMapper;
import net.zethmayr.benjamin.spring.common.mapper.base.Mapper;
import net.zethmayr.benjamin.spring.common.repository.base.MapperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import static net.gribnit.pester.persistence.PersistenceNames.GENERATOR_TEMPLATES;

@Service
public class GeneratorTemplateRepository extends MapperRepository<RelatedString, Integer> {
    public GeneratorTemplateRepository(final @Autowired JdbcTemplate jdbcTemplate) {
        super(jdbcTemplate, new RelatedStringMapper(GENERATOR_TEMPLATES), RelatedStringMapper.VALUE_ROW_ID);
    }
}
