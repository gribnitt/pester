package net.gribnit.pester.persistence.repositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.gribnit.pester.domain.definitions.UserDefinition;
import net.gribnit.pester.persistence.mappers.UserMapper;
import net.zethmayr.benjamin.spring.common.repository.base.JoiningRepository;
import net.zethmayr.benjamin.spring.common.repository.base.MapperRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class UserRepository extends JoiningRepository<UserDefinition, Integer> {

    public static class CoreRepository extends MapperRepository<UserDefinition, Integer> {
        private CoreRepository(final JdbcTemplate jdbcTemplate) {
            super(jdbcTemplate, new UserMapper.CoreMapper(), UserMapper.ID);
        }
    }

    public UserRepository(final @Autowired JdbcTemplate jdbcTemplate, final @Autowired ObjectMapper om, final @Autowired ContactMethodRepository contactMethods) {
        super(jdbcTemplate, new UserMapper(om), new CoreRepository(jdbcTemplate), contactMethods);
    }
}
