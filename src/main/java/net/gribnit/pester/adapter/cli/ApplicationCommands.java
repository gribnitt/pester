package net.gribnit.pester.adapter.cli;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.zethmayr.benjamin.spring.common.cli.CommandParser;
import net.zethmayr.benjamin.spring.common.cli.SubCommandBinding;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ApplicationCommands implements CommandLineRunner {
    private final ConfigurableApplicationContext context;

    @Value("${shibboleth}")
    private String shibboleth;

    public ApplicationCommands(final @Autowired ConfigurableApplicationContext context) {
        this.context = context;
    }

    @Override
    public void run(final String... args) throws Exception {
        LOG.info("shibboleth is {}", shibboleth);
        val commandParser = new CommandParser() {{
            bindings.put("exit", (a) -> SpringApplication.exit(context, () -> a.length == 1 ? Integer.valueOf(a[0]) : 0));
            bindings.put("dump", (a) -> LOG.info("context is {}", context));
        }};
        val entryParser = new CommandParser() {{
            bindings.put("app", SubCommandBinding.of(commandParser));
        }};
        entryParser.runCommands(entryParser.parseArgs(args));
    }
}
