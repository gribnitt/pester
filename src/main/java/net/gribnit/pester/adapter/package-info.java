/**
 * Adapters of various kinds.
 *
 * Generally speaking, nothing outside this package hierarchy should depend on anything in it.
 */
package net.gribnit.pester.adapter;