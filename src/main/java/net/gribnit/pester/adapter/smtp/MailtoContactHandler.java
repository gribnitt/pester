package net.gribnit.pester.adapter.smtp;

import lombok.extern.slf4j.Slf4j;
import net.gribnit.pester.domain.facilities.ContactMethodHandler;
import net.gribnit.pester.domain.facilities.KnownMethodFederations;
import net.gribnit.pester.domain.facilities.Message;
import net.gribnit.pester.domain.facilities.ProtocolRegistry;
import net.gribnit.pester.exception.SendFailedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.Future;

@Service
@Slf4j
public class MailtoContactHandler implements ContactMethodHandler {
    public static final String MAILTO = "mailto";

    private final MailtoHandlerConfig config;

    public MailtoContactHandler(final @Autowired ProtocolRegistry registry, final @Autowired MailtoHandlerConfig config) {
        this.config = config;
        registry.registerProtocolHandler(this);
    }

    @Override
    public KnownMethodFederations getFederation() {
        return KnownMethodFederations.EMAIL;
    }

    @Override
    public String getPrefix() {
        return MAILTO;
    }

    @Override
    public Future<SendFailedException> send(Message message) {
        LOG.trace("send for message {}", message);
        return null;
    }
}
