package net.gribnit.pester.adapter.smtp;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.zethmayr.benjamin.spring.common.cli.CommandParser;
import net.zethmayr.benjamin.spring.common.cli.SubCommandBinding;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class OverrideSmtpCredentials implements CommandLineRunner {

    private final MailtoHandlerConfig config;

    public OverrideSmtpCredentials(final @Autowired MailtoHandlerConfig config) {
        this.config = config;
    }

    @Override
    public void run(final String... args) throws Exception {
        val subparser = new CommandParser() {{
            bindings.put("password", (a) -> {
                config.setPassword(a[0]);
                LOG.info("Overrode SMTP password");
            });
            bindings.put("username", (a) -> {
                config.setUsername(a[0]);
                LOG.info("Overrode SMTP username");
            });
            bindings.put("dump", (a) -> LOG.info("config is {}", config));
        }};
        val parser = new CommandParser() {{
            bindings.put("smtp", SubCommandBinding.of(subparser));
        }};
        parser.runCommands(parser.parseArgs(args));
        LOG.debug("Checked for SMTP overrides");
    }
}
