package net.gribnit.pester.adapter.smtp;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

@ConfigurationProperties("pester.smtp")
@Service
@Data
public class MailtoHandlerConfig {
    private String smtpHost;
    private String username;
    private String password;
    private int tlsPort;
    private int sslPort;
    private boolean tlsSslRequired;
}
