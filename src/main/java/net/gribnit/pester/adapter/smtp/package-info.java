/**
 * Mail contact method plugin for {@link net.gribnit.pester.domain.facilities.ContactMethodHandler}
 *
 * Should be limited in dependencies to {@link net.gribnit.pester.domain.facilities} and
 * {@link net.gribnit.pester.exception}. Nothing may depend directly on this package.
 */
package net.gribnit.pester.adapter.smtp;