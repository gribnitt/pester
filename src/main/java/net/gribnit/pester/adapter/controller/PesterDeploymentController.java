package net.gribnit.pester.adapter.controller;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.gribnit.pester.domain.definitions.DeployedPesterDefinition;
import net.gribnit.pester.domain.definitions.PesterDefinition;
import net.gribnit.pester.exception.ServiceException;
import net.gribnit.pester.application.service.PesterDeploymentCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.PATCH;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@Controller
@Slf4j
@RequestMapping(path = "/pester/at")
public class PesterDeploymentController {

    private final PesterDeploymentCrudService deploymentService;

    public PesterDeploymentController(final @Autowired PesterDeploymentCrudService deploymentService) {
        this.deploymentService = deploymentService;
    }

    @ResponseBody
    @GetMapping(produces = APPLICATION_JSON_UTF8_VALUE)
    public List<DeployedPesterDefinition> allDeployedPesters() {
        LOG.trace("GET /at");
        val all = deploymentService.getAll();
        LOG.trace("all is {}", all);
        return all;
    }

    @ResponseBody
    @GetMapping(path = "{user}", produces = APPLICATION_JSON_UTF8_VALUE)
    public List<PesterDefinition> deployedPestersByUser(final @PathVariable("user") String userName) throws ServiceException {
        LOG.trace("GET /at/{}", userName);
        return deploymentService.getForUser(userName);
    }

    @ResponseBody
    @DeleteMapping(path = "{pesterId}")
    public void deleteDeployedPester(final @PathVariable("pesterId") int pesterId) throws ServiceException {
        LOG.trace("DELETE /at/{}", pesterId);
        deploymentService.delete(pesterId);
    }

    @ResponseBody
    @PostMapping(path = "{user}", consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public PesterDefinition deployPesterAtUser(final @PathVariable("user") String userName, final @RequestBody PesterDefinition pester) throws ServiceException {
        return deploymentService.deploy(userName, pester);
    }

    @ResponseBody
    @RequestMapping(method = {PATCH, PUT}, path = "{pesterId}", consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public PesterDefinition modifyDeployedPester(final @PathVariable("pesterId") int pesterId, final @RequestBody PesterDefinition pester) throws ServiceException {
        return deploymentService.modify(pesterId, pester);
    }
}
