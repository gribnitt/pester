package net.gribnit.pester.adapter.controller;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.gribnit.pester.domain.definitions.GeneratorDefinition;
import net.gribnit.pester.exception.ServiceException;
import net.gribnit.pester.application.service.PesterGeneratorsCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.PATCH;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@Controller
@Slf4j
@RequestMapping(path = "/pester/with")
public class PesterGeneratorsController {

    private final PesterGeneratorsCrudService generators;

    public PesterGeneratorsController(final @Autowired PesterGeneratorsCrudService generators) {
        this.generators = generators;
    }

    @ResponseBody
    @GetMapping(produces = APPLICATION_JSON_UTF8_VALUE)
    public List<GeneratorDefinition> allGenerators() {
        LOG.trace("GET /generators");
        val all = generators.getAll();
        LOG.trace("all is {}", all);
        return all;
    }

    @ResponseBody
    @GetMapping(path = "{generator}", produces = APPLICATION_JSON_UTF8_VALUE)
    public GeneratorDefinition generatorByName(final @PathVariable("generator") String generator) throws ServiceException {
        LOG.trace("GET /generators/{}", generator);
        return generators.getOne(generator);
    }

    @ResponseBody
    @DeleteMapping(path = "{generator}")
    public void deleteGenerator(final @PathVariable("generator") String generator) throws ServiceException {
        LOG.trace("DELETE /generators/{}", generator);
        generators.delete(generator);
    }

    @ResponseBody
    @PostMapping(path = "{generator}", consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public GeneratorDefinition createGenerator(final @PathVariable("generator") String generatorName, final @RequestBody GeneratorDefinition generator) throws ServiceException {
        return generators.create(generatorName, generator);
    }

    @ResponseBody
    @RequestMapping(method = {PATCH, PUT}, path = "{generator}", consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public GeneratorDefinition modifyGenerator(final @PathVariable("generator") String generatorName, final @RequestBody GeneratorDefinition generator) throws ServiceException {
        return generators.modify(generatorName, generator);
    }
}
