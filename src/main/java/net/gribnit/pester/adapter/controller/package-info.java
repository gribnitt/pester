/**
 * HTTP adapters i.e. Spring Boot Controllers.
 *
 * As usual, nothing should ever be depending on a controller.
 */
package net.gribnit.pester.adapter.controller;