package net.gribnit.pester.adapter.controller;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.gribnit.pester.domain.definitions.UserDefinition;
import net.gribnit.pester.exception.ServiceException;
import net.gribnit.pester.application.service.PesterUsersCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.PATCH;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@Controller
@Slf4j
@RequestMapping(path = "/pester/users")
public class PesterUsersController {

    private final PesterUsersCrudService usersService;

    public PesterUsersController(final @Autowired PesterUsersCrudService usersService) {
        this.usersService = usersService;
    }

    @ResponseBody
    @GetMapping(produces = APPLICATION_JSON_UTF8_VALUE)
    public List<UserDefinition> allUsers() {
        LOG.trace("GET /users");
        val all = usersService.getAll();
        LOG.trace("all is {}", all);
        return all;
    }

    @ResponseBody
    @GetMapping(path = "{user}", produces = APPLICATION_JSON_UTF8_VALUE)
    public UserDefinition getUser(final @PathVariable("user") String userName) throws ServiceException {
        LOG.trace("GET /users/{}", userName);
        return usersService.getOne(userName);
    }

    @ResponseBody
    @DeleteMapping(path = "{user}")
    public void deleteUser(final @PathVariable("user") String userName) throws ServiceException {
        LOG.trace("DELETE /users/{}", userName);
        usersService.delete(userName);
    }

    @ResponseBody
    @PostMapping(path = "{user}", consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public UserDefinition createUser(final @PathVariable("user") String userName, final @RequestBody UserDefinition user) throws ServiceException {
        return usersService.create(userName, user);
    }

    @ResponseBody
    @RequestMapping(method = {PATCH, PUT}, path = "{user}", consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public UserDefinition modifyUser(final @PathVariable("user") String userName, final @RequestBody UserDefinition user) throws ServiceException {
        return usersService.modify(userName, user);
    }
}
