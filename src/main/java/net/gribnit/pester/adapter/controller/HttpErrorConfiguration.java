package net.gribnit.pester.adapter.controller;

import net.gribnit.pester.exception.CreationConflictException;
import net.gribnit.pester.exception.ModificationConflictException;
import net.gribnit.pester.exception.NotFoundException;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Configuration
public class HttpErrorConfiguration {
    @ControllerAdvice
    public static class ErrorHandler extends ResponseEntityExceptionHandler {
        @ExceptionHandler(IllegalArgumentException.class)
        protected ResponseEntity<Object> handleBadArguments(final RuntimeException re, final WebRequest request) {
            return handleExceptionInternal(re, re.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
        }

        @ExceptionHandler(NotFoundException.class)
        protected ResponseEntity<Object> handleNotFound(final NotFoundException nfe, final WebRequest request) {
            return handleExceptionInternal(nfe, nfe.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND, request);
        }

        @ExceptionHandler(CreationConflictException.class)
        protected ResponseEntity<Object> handleCreationConflict(final CreationConflictException cce, final WebRequest request) {
            return handleExceptionInternal(cce, cce.getMessage(), new HttpHeaders(), HttpStatus.CONFLICT, request);
        }

        @ExceptionHandler(ModificationConflictException.class)
        protected ResponseEntity<Object> handleModificationConflict(final ModificationConflictException mce, final WebRequest request) {
            return handleExceptionInternal(mce, mce.getMessage(), new HttpHeaders(), HttpStatus.CONFLICT, request);
        }
    }
}
