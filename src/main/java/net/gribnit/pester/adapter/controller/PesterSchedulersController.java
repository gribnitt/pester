package net.gribnit.pester.adapter.controller;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.gribnit.pester.domain.definitions.SchedulerDefinition;
import net.gribnit.pester.exception.ServiceException;
import net.gribnit.pester.application.service.PesterSchedulersCrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.PATCH;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@Controller
@Slf4j
@RequestMapping(path = "/pester/by")
public class PesterSchedulersController {

    private final PesterSchedulersCrudService schedulers;

    public PesterSchedulersController(final @Autowired PesterSchedulersCrudService schedulers) {
        this.schedulers = schedulers;
    }

    @ResponseBody
    @GetMapping(produces = APPLICATION_JSON_UTF8_VALUE)
    public List<SchedulerDefinition> allSchedulers() {
        LOG.trace("GET /schedulers");
        val all = schedulers.getAll();
        LOG.trace("all is {}", all);
        return all;
    }

    @ResponseBody
    @GetMapping(path = "{scheduler}", produces = APPLICATION_JSON_UTF8_VALUE)
    public SchedulerDefinition schedulerByName(final @PathVariable("scheduler") String scheduler) throws ServiceException {
        LOG.trace("GET /schedulers/{}", scheduler);
        return schedulers.getOne(scheduler);
    }

    @ResponseBody
    @DeleteMapping(path = "{scheduler}")
    public void deleteScheduler(final @PathVariable("scheduler") String scheduler) throws ServiceException {
        LOG.trace("DELETE /schedulers/{}", scheduler);
        schedulers.delete(scheduler);
    }

    @ResponseBody
    @PostMapping(path = "{scheduler}", consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public SchedulerDefinition createScheduler(final @PathVariable("scheduler") String schedulerName, final @RequestBody SchedulerDefinition scheduler) throws ServiceException {
        return schedulers.create(schedulerName, scheduler);
    }

    @ResponseBody
    @RequestMapping(method = {PATCH, PUT}, path = "{scheduler}", consumes = APPLICATION_JSON_UTF8_VALUE, produces = APPLICATION_JSON_UTF8_VALUE)
    public SchedulerDefinition modifyScheduler(final @PathVariable("scheduler") String schedulerName, final @RequestBody SchedulerDefinition scheduler) throws ServiceException {
        return schedulers.modify(schedulerName, scheduler);
    }
}
