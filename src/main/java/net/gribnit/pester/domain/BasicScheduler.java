package net.gribnit.pester.domain;

import lombok.val;
import net.gribnit.pester.utility.ParamsMap;

import java.util.HashMap;
import java.util.Map;

public enum BasicScheduler {
    PERIODIC {
        @Override
        public boolean requiredParametersPresent(final ParamsMap each) {
            val keys = each.keySet();
            return keys.contains(TIMING);
        }
    };

    public static final String TIMING = "timing";

    private static final Map<String, BasicScheduler> byName = new HashMap<>();
    static {
        for (val each : BasicScheduler.values()) {
            byName.put(each.name().toLowerCase(), each);
        }
    }

    public static BasicScheduler forName(final String name) {
        return byName.get(name.toLowerCase());
    }

    public abstract boolean requiredParametersPresent(final ParamsMap each);
}
