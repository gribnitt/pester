/**
 * The actual problem domain and solutions.
 *
 * This package is not allowed to have any outside dependencies in {@link net.gribnit.pester}
 * except for "child" packages.
 */
package net.gribnit.pester.domain;