package net.gribnit.pester.domain.definitions;

import net.zethmayr.benjamin.spring.common.util.ListBuilder;

import java.util.ArrayList;

public class BasicSchedulerList extends ArrayList<BasicSchedulerDefinition> {
    public static ListBuilder<BasicSchedulerDefinition, BasicSchedulerList> builder() {
        return ListBuilder.on(new BasicSchedulerList());
    }
}