package net.gribnit.pester.domain.definitions;

import lombok.Data;
import lombok.experimental.Accessors;
import lombok.val;
import net.zethmayr.benjamin.spring.common.util.ListBuilder;

import java.util.List;
import java.util.function.Consumer;

@Data
@Accessors(chain = true)
public class UserDefinition implements Definition {
    protected Integer id;
    protected String name;
    protected ContactMethodDefinitionList contactMethods = new ContactMethodDefinitionList();

    public UserDefinition from(final UserDefinition original) {
        this.setId(original.getId());
        this.setName(original.getName());
        this.setContactMethods(ListBuilder.on(new ContactMethodDefinitionList())
                .add(original.getContactMethods())
                .toEach(e -> new ContactMethodDefinition().from(e))
                .build()
        );
        return this;
    }

    @SafeVarargs
    public final UserDefinition withContactMethods(final Consumer<ListBuilder<ContactMethodDefinition, ContactMethodDefinitionList>>... mutators) {
        val builder = ListBuilder.on(contactMethods);
        for (val each : mutators) {
            each.accept(builder);
        }
        return this;
    }
}
