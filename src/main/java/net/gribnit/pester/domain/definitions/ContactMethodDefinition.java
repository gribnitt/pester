package net.gribnit.pester.domain.definitions;

import lombok.Data;
import lombok.experimental.Accessors;
import net.gribnit.pester.utility.ParamsBuilder;
import net.gribnit.pester.utility.ParamsMap;
import net.zethmayr.benjamin.spring.common.util.MapBuilder;

import java.util.function.Consumer;

import static net.gribnit.pester.utility.ParamsBuilder.paramsBuilder;

@Data
@Accessors(chain = true)
public class ContactMethodDefinition implements Definition {
    public static final String IS_DEFAULT = "default";

    protected Integer id;
    protected Integer userId;
    protected String name;
    protected String url;
    protected ParamsMap params = new ParamsMap();

    @SafeVarargs
    public final ContactMethodDefinition withParams(final Consumer<MapBuilder<String, Object, ParamsMap>>... paramMutators) {
        ParamsBuilder.onParams(params, paramMutators);
        return this;
    }

    public ContactMethodDefinition from(final ContactMethodDefinition original) {
        this.setId(original.getId());
        this.setUserId(original.getUserId());
        this.setName(original.getName());
        this.setUrl(original.getUrl());
        this.setParams(paramsBuilder().put(original.getParams()).build());
        return this;
    }
}
