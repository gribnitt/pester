package net.gribnit.pester.domain.definitions;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.time.Instant;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@Accessors(chain = true)
public class DeployedPesterDefinition extends PesterDefinition<DeployedPesterDefinition> {
    private Integer userId;
    private Instant deployed;

    @Override
    public DeployedPesterDefinition from(final PesterDefinition original) {
        super.from(original);
        return this;
    }

    public DeployedPesterDefinition from(final DeployedPesterDefinition original) {
        super.from(original);
        setUserId(original.getUserId());
        setDeployed(original.getDeployed());
        return this;
    }
}
