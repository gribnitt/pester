package net.gribnit.pester.domain.definitions;

import lombok.Data;
import lombok.experimental.Accessors;
import lombok.val;
import net.zethmayr.benjamin.spring.common.util.ListBuilder;

import java.util.ArrayList;
import java.util.function.Consumer;

@Data
@Accessors(chain = true)
public class SchedulerDefinition implements Definition<SchedulerDefinition> {
    private Integer id;
    private String name;
    private String generator;
    private BasicSchedulerList schedulers = new BasicSchedulerList();

    @SafeVarargs
    public final SchedulerDefinition withSchedulers(final Consumer<ListBuilder<BasicSchedulerDefinition, BasicSchedulerList>>... schedulersMutators) {
        val builder = ListBuilder.<BasicSchedulerDefinition, BasicSchedulerList>on(schedulers);
        for (val each : schedulersMutators) {
            each.accept(builder);
        }
        return this;
    }
}
