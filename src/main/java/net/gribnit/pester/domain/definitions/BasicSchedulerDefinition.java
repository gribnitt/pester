package net.gribnit.pester.domain.definitions;

import lombok.Data;
import lombok.experimental.Accessors;
import net.gribnit.pester.utility.ParamsBuilder;
import net.gribnit.pester.utility.ParamsMap;
import net.zethmayr.benjamin.spring.common.util.MapBuilder;

import java.util.LinkedHashMap;
import java.util.function.Consumer;

@Data
@Accessors(chain = true)
public class BasicSchedulerDefinition implements Definition<BasicSchedulerDefinition> {
    private Integer id;
    private Integer schedulerId;
    private String type;
    private String generator;
    private ParamsMap params = new ParamsMap();

    @SafeVarargs
    public final BasicSchedulerDefinition withParams(final Consumer<MapBuilder<String, Object, ParamsMap>>... paramsMutators) {
        ParamsBuilder.onParams(params, paramsMutators);
        return this;
    }
}
