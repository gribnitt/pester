package net.gribnit.pester.domain.definitions;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import net.gribnit.pester.utility.ParamsBuilder;
import net.gribnit.pester.utility.ParamsMap;
import net.zethmayr.benjamin.spring.common.util.MapBuilder;

import java.util.function.Consumer;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class PesterDefinition<C extends PesterDefinition> implements Definition<C> {
    private Integer id;
    private String scheduler;
    private String generator;
    private ParamsMap params = new ParamsMap();

    public PesterDefinition from(final PesterDefinition original) {
        setId(original.getId());
        setScheduler(original.getScheduler());
        setGenerator(original.getGenerator());
        setParams(original.getParams());
        return this;
    }

    @SafeVarargs
    @SuppressWarnings("unchecked")
    public final C withParams(final Consumer<MapBuilder<String, Object, ParamsMap>>... paramMutators) {
        ParamsBuilder.onParams(params, paramMutators);
        return (C)this;
    }
}