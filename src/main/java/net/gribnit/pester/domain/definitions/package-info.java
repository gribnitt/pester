/**
 * The API-level types.
 *
 * This package is not allowed to have any outside dependencies in {@link net.gribnit.pester}
 * except from {@link net.gribnit.pester.utility}
 */
package net.gribnit.pester.domain.definitions;