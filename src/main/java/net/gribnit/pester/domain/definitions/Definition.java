package net.gribnit.pester.domain.definitions;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_EMPTY;

@JsonInclude(value = NON_EMPTY, content = NON_EMPTY)
@JsonNaming(value = PropertyNamingStrategy.KebabCaseStrategy.class)
interface Definition<C extends Definition> {
}
