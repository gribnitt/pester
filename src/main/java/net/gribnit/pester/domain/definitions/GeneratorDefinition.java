package net.gribnit.pester.domain.definitions;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import net.gribnit.pester.utility.StringList;

import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class GeneratorDefinition implements Definition {
    private Integer id;
    private String name;
    private StringList templates = new StringList();
}
