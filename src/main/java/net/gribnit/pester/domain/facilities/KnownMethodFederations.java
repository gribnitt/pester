package net.gribnit.pester.domain.facilities;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

import static net.gribnit.pester.domain.definitions.ContactMethodDefinition.IS_DEFAULT;

public enum KnownMethodFederations {
    EMAIL,
    INSTANT_MESSAGE,
    PHONE_TEXT,
    PHONE_VOICE,
    PHYSICAL("transit");

    private final Set<String> knownParameters = new TreeSet<>();

    KnownMethodFederations(final String... knownParameters) {
        this.knownParameters.addAll(Arrays.asList(knownParameters));
        this.knownParameters.add(IS_DEFAULT);
    }

    public Set<String> knownParameters() {
        return Collections.unmodifiableSet(knownParameters);
    }

}
