package net.gribnit.pester.domain.facilities;

import net.gribnit.pester.exception.SendFailedException;

import java.util.concurrent.Future;

public interface ContactMethodHandler {
    KnownMethodFederations getFederation();
    String getPrefix();

    Future<SendFailedException> send(final Message message);
}
