/**
 * Statically-available model use cases
 *
 * This package is not allowed to have any outside dependencies in {@link net.gribnit.pester}
 * except from {@link net.gribnit.pester.domain} and "child" packages.
 */
package net.gribnit.pester.domain.facilities;