package net.gribnit.pester.domain.facilities;

import net.gribnit.pester.exception.AlreadyRegisteredException;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.stream.Collectors;

public class ProtocolRegistry {
    private final ConcurrentSkipListMap<String, ContactMethodHandler> knownProtocols = new ConcurrentSkipListMap<>();

    public void registerProtocolHandler(final ContactMethodHandler protocolHandler) {
        final ContactMethodHandler existing;
        if ((existing = knownProtocols.putIfAbsent(protocolHandler.getPrefix(), protocolHandler)) != null) {
            throw AlreadyRegisteredException.collidesWith(existing);
        }
    }

    public Optional<ContactMethodHandler> findRegisteredHandler(final String protocol) {
        return Optional.ofNullable(knownProtocols.get(protocol));
    }

    public List<ContactMethodHandler> findRegisteredHandlers(final KnownMethodFederations federation) {
        return knownProtocols.values().stream().filter(p -> p.getFederation() == federation).collect(Collectors.toList());
    }
}
