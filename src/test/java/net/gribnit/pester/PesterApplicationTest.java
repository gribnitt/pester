package net.gribnit.pester;

import net.zethmayr.benjamin.spring.common.repository.DefaultSchemaService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static net.gribnit.pester.TestConstants.TEST_PROFILE;
import static org.hamcrest.Matchers.isA;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(TEST_PROFILE)
public class PesterApplicationTest {
    @Autowired
    private PesterApplication underTest;

    @Autowired
    private DefaultSchemaService schemaService;

    @Test
    public void wires() {
        assertThat(underTest, isA(PesterApplication.class));
        assertThat(schemaService, isA(DefaultSchemaService.class));
    }
}
