package net.gribnit.pester.domain.definitions;

import com.fasterxml.jackson.core.util.DefaultIndenter;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Before;

import static net.gribnit.pester.TestConstants.EXAMPLE_GENERATOR;
import static net.gribnit.pester.TestConstants.EXAMPLE_SCHEDULER;
import static net.gribnit.pester.TestConstants.quoted;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

public abstract class DefinitionTest {
    protected ObjectWriter ow;
    protected ObjectMapper om;

    protected static final class PrettyPrinter extends DefaultPrettyPrinter {
        public PrettyPrinter() {
            super();
            indentArraysWith(DefaultIndenter.SYSTEM_LINEFEED_INSTANCE);
        }
    }

    @Before
    public void setUp() {
        this.om = new ObjectMapper();
        om.enable(SerializationFeature.INDENT_OUTPUT);
        enableArrayIndentation();
    }

    protected void enableArrayIndentation() {
        this.ow = om.writer(new PrettyPrinter());
    }

    protected void disableArrayIndentation() {
        this.ow = om.writerWithDefaultPrettyPrinter();
    }

    protected void assertContainsCorePesterFields(final String mapped) {
        assertThat(mapped, containsString(quoted("scheduler")));
        assertThat(mapped, containsString(quoted("generator")));
        assertThat(mapped, containsString(quoted(EXAMPLE_GENERATOR)));
        assertThat(mapped, containsString(quoted(EXAMPLE_SCHEDULER)));
    }
}
