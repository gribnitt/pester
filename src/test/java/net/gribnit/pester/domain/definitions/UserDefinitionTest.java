package net.gribnit.pester.domain.definitions;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

import static net.gribnit.pester.TestConstants.exampleUserDefinition;
import static net.gribnit.pester.TestConstants.moreMethods;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@Slf4j
public class UserDefinitionTest extends DefinitionTest {
    @Test
    public void canReproduceExample() throws Exception {
        disableArrayIndentation();
        val underTest = exampleUserDefinition().withContactMethods(moreMethods());

        val mapped = ow.writeValueAsString(underTest);
        LOG.info("mapped\n{}\nto\n{}", underTest, mapped);
    }

    @Test
    public void testEqualsAndHashCode() {
        UserDefinition a, b;
        int[][] hashes = new int[7][2];
        int i = 0;

        assertEquals(a = new UserDefinition(), b = new UserDefinition());
        hashes[i][0] = a.hashCode();
        hashes[i++][1] = b.hashCode();
        a.setId(1);
        assertNotEquals(a, b);
        hashes[i][0] = a.hashCode();
        hashes[i++][1] = b.hashCode();
        b.setId(1);
        assertEquals(a, b);
        hashes[i][0] = a.hashCode();
        hashes[i++][1] = b.hashCode();

        ContactMethodDefinition am, bm;
        am = new ContactMethodDefinition();
        bm = new ContactMethodDefinition();

        a.withContactMethods(l -> l.add(am));
        assertNotEquals(a, b);
        hashes[i][0] = a.hashCode();
        hashes[i++][1] = b.hashCode();
        b.withContactMethods(l -> l.add(bm));
        assertEquals(a, b);
        hashes[i][0] = a.hashCode();
        hashes[i++][1] = b.hashCode();
        am.setId(1);
        assertNotEquals(a, b);
        hashes[i][0] = a.hashCode();
        hashes[i++][1] = b.hashCode();
        bm.setId(1);
        assertEquals(a, b);
        hashes[i][0] = a.hashCode();
        hashes[i++][1] = b.hashCode();

        // safe
        assertEquals(hashes[0][0], hashes[0][1]);
        assertEquals(hashes[2][0], hashes[2][1]);
        assertEquals(hashes[4][0], hashes[4][1]);
        assertEquals(hashes[6][0], hashes[6][1]);
        // unsafe
        assertNotEquals(hashes[1][0], hashes[1][1]);
        assertNotEquals(hashes[3][0], hashes[3][1]);
        assertNotEquals(hashes[5][0], hashes[5][1]);
    }
}
