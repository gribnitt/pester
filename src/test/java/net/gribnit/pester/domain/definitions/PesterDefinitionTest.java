package net.gribnit.pester.domain.definitions;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.junit.Test;

import java.util.Arrays;

import static net.gribnit.pester.TestConstants.examplePesterDefinition;
import static net.gribnit.pester.TestConstants.quoted;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;

@Slf4j
public class PesterDefinitionTest extends DefinitionTest {

    @Test
    public void canReproduceFirstExample() throws Exception {
        val underTest = examplePesterDefinition();
        val mapped = ow.writeValueAsString(underTest);
        LOG.info("mapped\n{}\nto\n{}", underTest, mapped);
        assertContainsCorePesterFields(mapped);
        assertThat(mapped, not(containsString(quoted("id"))));
        assertThat(mapped, not(containsString(quoted("withParams"))));
    }

    @Test
    public void canReproduceSecondExample() throws Exception {
        disableArrayIndentation();
        val underTest = examplePesterDefinition()
                .withParams((m) -> m.put("poke", "random-hourly"));
        val mapped = ow.writeValueAsString(underTest);
        LOG.info("mapped\n{}\nto\n{}", underTest, mapped);
        assertContainsCorePesterFields(mapped);
        assertThat(mapped, not(containsString(quoted("id"))));
        assertThat(mapped, containsString(quoted("params")));
        assertThat(mapped, containsString(quoted("poke")));
        assertThat(mapped, containsString(quoted("random-hourly")));
    }

    @Test
    public void canCopy() throws Exception {
        val underTest = new PesterDefinition().from(examplePesterDefinition())
                .setId(42);
        val mapped = ow.writeValueAsString(underTest);
        LOG.info("mapped\n{}\nto\n{}", underTest, mapped);
        assertContainsCorePesterFields(mapped);
        assertThat(mapped, containsString(quoted("id")));
        assertThat(mapped, containsString("42"));
        assertThat(mapped, not(containsString(quoted("withParams"))));
    }

    @Test
    public void testEqualsAndHashCode() {
        PesterDefinition a, b;
        int[][] hashes = new int[3][2];
        int i = 0;

        assertEquals(a = new PesterDefinition(), b = new PesterDefinition());
        hashes[i][0] = a.hashCode();
        hashes[i++][1] = b.hashCode();
        a.setId(1);
        assertNotEquals(a, b);
        hashes[i][0] = a.hashCode();
        hashes[i++][1] = b.hashCode();
        b.setId(1);
        assertEquals(a, b);
        hashes[i][0] = a.hashCode();
        hashes[i++][1] = b.hashCode();

        // safe
        assertEquals(hashes[0][0], hashes[0][1]);
        assertEquals(hashes[2][0], hashes[2][1]);
        // unsafe
        assertNotEquals(hashes[1][0], hashes[1][1]);
    }
}
