package net.gribnit.pester.domain.definitions;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.junit.Test;

import static net.gribnit.pester.TestConstants.exampleSchedulerDefinition;

@Slf4j
public class SchedulerDefinitionTest extends DefinitionTest {
    @Test
    public void canReproduceExample() throws Exception {
        val underTest = exampleSchedulerDefinition();
        val mapped = ow.writeValueAsString(underTest);
        LOG.info("mapped\n{}\nto\n{}", underTest, mapped);
    }

    @Test
    public void canReproduceExampleWithSetSchedulers() throws Exception {
         final SchedulerDefinition underTest = new SchedulerDefinition()
                .setId(1)
                .setName("damn-you-joe")
                .setGenerator("panic-attack")
                .setSchedulers(BasicSchedulerList.builder().add(
                        new BasicSchedulerDefinition().setType("periodic").withParams((m) -> m
                                .put("_random", "random-hourly")
                        ),
                        new BasicSchedulerDefinition().setType("periodic").withParams((m) -> m
                                .put("poke", "daily@11:47")
                        ),
                        new BasicSchedulerDefinition().setType("periodic").withParams((m) -> m
                                .put("moreIm", "daily@16:50")
                        ),
                        new BasicSchedulerDefinition().setType("periodic").withParams((m) -> m
                                .put("mailto://boss@sch.mo", "random-weekly")
                        ).setGenerator("concern-troll")
                ).build());
        val mapped = ow.writeValueAsString(underTest);
        LOG.info("mapped\n{}\nto\n{}", underTest, mapped);
    }
}
