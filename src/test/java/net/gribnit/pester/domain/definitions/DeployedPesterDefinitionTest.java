package net.gribnit.pester.domain.definitions;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.junit.Test;

import static net.gribnit.pester.TestConstants.examplePesterDefinition;
import static net.gribnit.pester.TestConstants.quoted;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isA;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;

@Slf4j
public class DeployedPesterDefinitionTest extends DefinitionTest {
    @Test
    public void canUpCopyAndAddUserId() throws Exception {
        val underTest = new DeployedPesterDefinition()
                .from(examplePesterDefinition())
                .setUserId(23);

        val mapped = ow.writeValueAsString(underTest);
        LOG.info("mapped\n{}\nto\n{}", underTest, mapped);

        assertThat(underTest, isA(DeployedPesterDefinition.class));
        assertContainsCorePesterFields(mapped);
        assertThat(mapped, not(containsString(quoted("params"))));
        assertThat(mapped, not(containsString(quoted("id"))));
        assertContainsUserId(mapped);
    }

    private void assertContainsUserId(final String mapped) {
        assertThat(mapped, containsString(quoted("user-id")));
        assertThat(mapped, containsString("23"));
    }

    @Test
    public void canCopyIncludingUserId() throws Exception {
        val underTest = new DeployedPesterDefinition().from(new DeployedPesterDefinition().from(examplePesterDefinition()).setUserId(23));
        assertThat(underTest.getUserId(), is(23));
    }

    @Test
    public void canAddParams() throws Exception {
        val underTest = new DeployedPesterDefinition()
                .<DeployedPesterDefinition>withParams((m) -> m.put("poke", "random-hourly"))
                .setUserId(23);

        val mapped = ow.writeValueAsString(underTest);
        LOG.info("mapped\n{}\nto\n{}", underTest, mapped);

        assertThat(underTest, isA(DeployedPesterDefinition.class));
        assertThat(mapped, containsString(quoted("params")));
        assertThat(mapped, containsString(quoted("poke")));
        assertThat(mapped, containsString(quoted("random-hourly")));
        assertThat(mapped, not(containsString(quoted("id"))));
        assertContainsUserId(mapped);
    }

    @Test
    public void testEqualsAndHashCode() {
        DeployedPesterDefinition a, b;
        int[][] hashes = new int[5][2];
        int i = 0;

        assertEquals(a = new DeployedPesterDefinition(), b = new DeployedPesterDefinition());
        hashes[i][0] = a.hashCode();
        hashes[i++][1] = b.hashCode();
        a.setId(1);
        assertNotEquals(a, b);
        hashes[i][0] = a.hashCode();
        hashes[i++][1] = b.hashCode();
        b.setId(1);
        assertEquals(a, b);
        hashes[i][0] = a.hashCode();
        hashes[i++][1] = b.hashCode();

        a.setUserId(1);
        assertNotEquals(a, b);
        hashes[i][0] = a.hashCode();
        hashes[i++][1] = b.hashCode();
        b.setUserId(1);
        assertEquals(a, b);
        hashes[i][0] = a.hashCode();
        hashes[i++][1] = b.hashCode();

        // safe
        assertEquals(hashes[0][0], hashes[0][1]);
        assertEquals(hashes[2][0], hashes[2][1]);
        assertEquals(hashes[4][0], hashes[4][1]);
        // unsafe
        assertNotEquals(hashes[1][0], hashes[1][1]);
        assertNotEquals(hashes[3][0], hashes[3][1]);
    }
}
