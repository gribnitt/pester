package net.gribnit.pester.domain.definitions;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.gribnit.pester.utility.StringList;
import org.junit.Test;

import java.util.Arrays;

import static net.gribnit.pester.TestConstants.exampleGeneratorDefinition;
import static net.gribnit.pester.TestConstants.panicAttack;
import static org.hamcrest.Matchers.isA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertThat;

@Slf4j
public class GeneratorDefinitionTest extends DefinitionTest {
    @Test
    public void canReproduceExample() throws Exception {
        val underTest = exampleGeneratorDefinition();
        assertThat(underTest, isA(GeneratorDefinition.class));
        val mapped = ow.writeValueAsString(underTest);
        LOG.info("mapped\n{}\nto\n{}", underTest, mapped);
    }

    @Test
    public void canSerializeWithPlaceholders() throws Exception {
        val underTest = panicAttack();
        assertThat(underTest, isA(GeneratorDefinition.class));
        val mapped = ow.writeValueAsString(underTest);
        LOG.info("mapped\n{}\nto\n{}", underTest, mapped);
    }

    @Test
    public void testEqualsAndHashCode() {
        GeneratorDefinition a, b;
        int[][] hashes = new int[3][2];
        int i = 0;

        assertEquals(a = new GeneratorDefinition(), b = new GeneratorDefinition());
        hashes[i][0] = a.hashCode();
        hashes[i++][1] = b.hashCode();
        a.setId(1);
        assertNotEquals(a, b);
        hashes[i][0] = a.hashCode();
        hashes[i++][1] = b.hashCode();
        b.setId(1);
        assertEquals(a, b);
        hashes[i][0] = a.hashCode();
        hashes[i++][1] = b.hashCode();

        // safe
        assertEquals(hashes[0][0], hashes[0][1]);
        assertEquals(hashes[2][0], hashes[2][1]);
        // unsafe
        assertNotEquals(hashes[1][0], hashes[1][1]);
    }
}
