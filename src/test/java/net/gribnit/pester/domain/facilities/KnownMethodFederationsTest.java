package net.gribnit.pester.domain.facilities;

import net.gribnit.pester.domain.definitions.ContactMethodDefinition;
import org.junit.Test;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

public class KnownMethodFederationsTest {
    @Test
    public void emailHasExpectedKnownParameters() {
        assertThat(KnownMethodFederations.EMAIL.knownParameters(), contains(
                ContactMethodDefinition.IS_DEFAULT
        ));
    }

    @Test
    public void physicalHasExpectedKnownParameters() {
        assertThat(KnownMethodFederations.PHYSICAL.knownParameters(), contains(
                ContactMethodDefinition.IS_DEFAULT,
                "transit"
        ));
    }
}
