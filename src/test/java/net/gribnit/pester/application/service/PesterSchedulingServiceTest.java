package net.gribnit.pester.application.service;

import lombok.val;
import net.gribnit.pester.domain.definitions.DeployedPesterDefinition;
import net.gribnit.pester.domain.definitions.SchedulerDefinition;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import java.time.Clock;
import java.util.concurrent.ScheduledFuture;

import static net.gribnit.pester.TestConstants.exampleGeneratorDefinition;
import static net.gribnit.pester.TestConstants.examplePesterDefinition;
import static net.gribnit.pester.TestConstants.exampleSchedulerDefinition;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PesterSchedulingServiceTest {
    private PesterSchedulingService underTest;

    @Mock
    private PesterEvents pesterEvents;

    @Mock
    private PesterDeploymentCrudService deployedPesters;

    @Mock
    private PesterSchedulersCrudService pesterSchedules;

    @Mock
    private PesterGeneratorsCrudService pesterGenerators;

    @Mock
    private ThreadPoolTaskScheduler scheduler;

    private Clock clock;

    @Mock
    private PesterDeliveryService pesterDelivery;

    @Before
    public void setUp() throws Exception {
        clock = Clock.systemDefaultZone();
        underTest = new PesterSchedulingService(
                pesterEvents, deployedPesters, pesterSchedules, pesterGenerators, scheduler, clock, pesterDelivery
        );
        when(pesterSchedules.getOne(any())).thenReturn(
                withIds(exampleSchedulerDefinition(), 1)
        );
        when(pesterGenerators.getOne(any())).thenReturn(
                exampleGeneratorDefinition()
                        .setId(2)
        );
        when(scheduler.scheduleAtFixedRate(any(), any(), anyLong())).thenReturn(
                mock(ScheduledFuture.class)
        );
    }

    private SchedulerDefinition withIds(final SchedulerDefinition definition, int startId) {
        definition.setId(startId++);
        for (val each : definition.getSchedulers()) {
            each.setId(startId++);
        }
        return definition;
    }

    @Test
    public void canCreateScheduler() {
        underTest.createSchedulerFor(new DeployedPesterDefinition().from(examplePesterDefinition().setId(0)));
        verify(scheduler, atLeastOnce()).scheduleAtFixedRate(any(), any(), anyLong());
    }

    @Test
    public void canCreateAndDeleteScheduler() {
        val deployed = new DeployedPesterDefinition().from(examplePesterDefinition().setId(0));
        underTest.createSchedulerFor(deployed);
        verify(scheduler, atLeastOnce()).scheduleAtFixedRate(any(), any(), anyLong());
        reset(scheduler);
        underTest.removeSchedulerFor(deployed);
        underTest.createSchedulerFor(deployed);
        verify(scheduler, atLeastOnce()).scheduleAtFixedRate(any(), any(), anyLong());
    }

    @Test
    public void doesNotDoubleSchedule() {
        val deployed = new DeployedPesterDefinition().from(examplePesterDefinition().setId(0));
        underTest.createSchedulerFor(deployed);
        verify(scheduler, atLeastOnce()).scheduleAtFixedRate(any(), any(), anyLong());
        underTest.createSchedulerFor(deployed);
        verifyNoMoreInteractions(scheduler);
    }

    @Test
    public void canParseMinutes() {
        assertThat(PesterSchedulingService.minutesFrom("00"), is(0));
        assertThat(PesterSchedulingService.minutesFrom("oo"), is(-1));
        assertThat(PesterSchedulingService.minutesFrom(":59"), is(59));
    }
}