package net.gribnit.pester.application.service;

import lombok.val;
import net.gribnit.pester.exception.CreationConflictException;
import net.gribnit.pester.exception.NotFoundException;
import net.gribnit.pester.persistence.repositories.ContactMethodParameterRepository;
import net.gribnit.pester.persistence.repositories.ContactMethodRepository;
import net.gribnit.pester.persistence.repositories.UserRepository;
import net.zethmayr.benjamin.spring.common.exception.TestFailedException;
import net.zethmayr.benjamin.spring.common.repository.DefaultSchemaService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static net.gribnit.pester.TestConstants.exampleUserDefinition;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isA;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class PesterUsersCrudServiceIT {
    @Autowired
    private PesterUsersCrudService underTest;

    @Autowired
    private DefaultSchemaService schemaService;

    @SpyBean
    private UserRepository persistedUsers;

    @SpyBean
    private ContactMethodRepository persistedContactMethods;

    @SpyBean
    private ContactMethodParameterRepository persistedMethodParams;

    @SpyBean
    private PesterEvents pesterEvents;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        schemaService.nuke(persistedUsers, persistedContactMethods, persistedMethodParams);
        schemaService.applySchemaFor(persistedUsers, persistedContactMethods, persistedMethodParams);
    }

    @Test
    public void wires() {
        assertThat(underTest, isA(PesterUsersCrudService.class));
    }

    @Test
    public void canCreateAndFindUserByName() throws Exception {
        val user = exampleUserDefinition();
        val post = underTest.create(user.getName(), user);
        assertThat(post, is(user));
        val read = underTest.getUser(post.getName());
        assertThat(read, is(user));
        val reread = persistedUsers.get(user.getId()).orElseThrow(TestFailedException::new);
        assertThat(read, is(reread));
    }

    @Test
    public void canCreateAndDeleteUserByName() throws Exception {
        val user = exampleUserDefinition();
        val post = underTest.create(user.getName(), user);
        assertThat(post, is(user));
        underTest.delete(user.getName());
        verify(pesterEvents).userDeleted(user);
        val reread = persistedUsers.get(user.getId());
        assertThat(reread.isPresent(), is(false));
    }

    @Test
    public void cannotCreateWhenNameExists() throws Exception {
        thrown.expect(CreationConflictException.class);
        val user = exampleUserDefinition();
        underTest.create(user.getName(), user);
        underTest.create(user.getName(), user);
    }

    @Test
    public void cannotDeleteNonexistentUser() throws Exception {
        thrown.expect(NotFoundException.class);
        underTest.delete("Steve");
    }

    @Test
    public void cannotGetNonexistentUser() throws Exception {
        thrown.expect(NotFoundException.class);
        underTest.getUser("Steve");
    }
}
