package net.gribnit.pester.application.service;

import lombok.val;
import net.gribnit.pester.adapter.smtp.MailtoContactHandler;
import net.gribnit.pester.domain.facilities.KnownMethodFederations;
import net.gribnit.pester.exception.AlreadyRegisteredException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProtocolRegistryServiceIT {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Autowired
    private ProtocolRegistryService underTest;

    @Autowired
    private MailtoContactHandler aRegistrant;

    @Test
    public void registersMailtoHandler() {
        val registered = underTest.findRegisteredHandler(MailtoContactHandler.MAILTO);
        assertTrue(registered.isPresent());
        val handler = registered.get();
        assertThat(handler, is(aRegistrant));
    }

    @Test
    public void registersEmailHandler() {
        val registered = underTest.findRegisteredHandlers(KnownMethodFederations.EMAIL);
        assertThat(registered, hasSize(1));
        val handler = registered.get(0);
        assertThat(handler, is(aRegistrant));
    }

    @Test
    public void cannotReregisterEmailHandler() {
        thrown.expect(AlreadyRegisteredException.class);
        underTest.registerProtocolHandler(aRegistrant);
    }
}
