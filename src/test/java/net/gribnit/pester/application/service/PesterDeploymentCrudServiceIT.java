package net.gribnit.pester.application.service;

import net.gribnit.pester.domain.definitions.DeployedPesterDefinition;
import net.gribnit.pester.domain.definitions.PesterDefinition;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static net.gribnit.pester.TestConstants.examplePesterDefinition;
import static net.gribnit.pester.TestConstants.exampleUserDefinition;
import static net.gribnit.pester.persistence.PersistenceNames.SUBJECT;
import static org.hamcrest.Matchers.isA;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class PesterDeploymentCrudServiceIT {
    @Autowired
    private PesterDeploymentCrudService underTest;

    @SpyBean
    private PesterUsersCrudService users;

    @MockBean
    private PesterEvents events;

    @Test
    public void wires() {
        assertThat(underTest, isA(PesterDeploymentCrudService.class));
    }

    @Test
    public void canDeployThenDeletePester() throws Exception {
        users.create("Joe", exampleUserDefinition());
        final PesterDefinition basePester = examplePesterDefinition().withParams((m) -> m.put(SUBJECT, "TPS Report"));
        final DeployedPesterDefinition pester = underTest.deploy("Joe", basePester);
        verify(events).pesterCreated(pester);
        underTest.delete(pester.getId());
        verify(events).pesterDeleted(pester);
    }
}
