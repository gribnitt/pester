package net.gribnit.pester.utility;


import lombok.val;
import net.zethmayr.benjamin.spring.common.util.ListBuilder;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

public class StringListTest {
    private StringList underTest;

    @Before
    public void setUp() {
        underTest = new StringList();
    }

    @Test
    public void canPopulate() {
        val builder = ListBuilder.on(underTest);
        underTest.add("one");
        builder.add("two");
        val relatedTwo = underTest.asRelatedStrings();
        assertThat(relatedTwo, hasSize(2));
        underTest.add("three");
        val related = underTest.asRelatedStrings();
        assertThat(related, hasSize(3));
    }
}
