package net.gribnit.pester.utility;

import lombok.val;
import net.zethmayr.benjamin.spring.common.util.MapBuilder;
import org.junit.Test;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.isA;
import static org.junit.Assert.assertThat;

public class ParamsBuilderTest {
    @Test
    public void canBuildNew() {
        val underTest = ParamsBuilder.paramsBuilder();
        assertThat(underTest, isA(MapBuilder.class));
        underTest.put("a", true).put("b", 7).put("c", "a");
        val built = underTest.build();
        assertThat(built, isA(ParamsMap.class));
        assertThat(built.values(), contains(true, 7, "a"));
    }

    @Test
    public void canMutateExisting() {
        val existing = ParamsBuilder.paramsBuilder().build();
        ParamsBuilder.onParams(existing, (underTest) ->
                underTest.put("a", true).put("b", 7).put("c", "a")
        );
        assertThat(existing.values(), contains(true, 7, "a"));
    }
}
