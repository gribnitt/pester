package net.gribnit.pester.persistence.repositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.zethmayr.benjamin.spring.common.repository.DefaultSchemaService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static net.gribnit.pester.TestConstants.exampleUserDefinition;
import static net.gribnit.pester.TestConstants.moreMethods;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isA;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class UserRepositoryIT {
    @Autowired
    private UserRepository underTest;

    @SpyBean
    private ContactMethodRepository contactMethods;

    @SpyBean
    private ContactMethodParameterRepository methodParameters;

    @Autowired
    private DefaultSchemaService schemaService;

    @Autowired
    private ObjectMapper om;

    @Before
    public void setUp() {
        schemaService.nuke(underTest.primary, contactMethods.primary, methodParameters);
        schemaService.applySchemaFor(underTest.primary, contactMethods.primary, methodParameters);
    }

    @Test
    public void wires() {
        assertThat(underTest, isA(UserRepository.class));
    }

    @Test
    public void canInsertThenRead() throws Exception {
        val original = exampleUserDefinition().withContactMethods(moreMethods());
        original.setId(null);
        LOG.info("original is {} ({})", om.writeValueAsString(original), original);
        val id = underTest.insert(original);
        LOG.info("after insert is {} ({})", om.writeValueAsString(original), original);
        assertThat(id, not(nullValue()));
        assertThat(id, is(original.getId()));
        val readReturn = underTest.get(id);
        assertThat(readReturn.isPresent(), is(true));
        val read = readReturn.get();
        LOG.info("read is {} ({})", om.writeValueAsString(read), read);
        assertThat(read, is(original));
    }
}
