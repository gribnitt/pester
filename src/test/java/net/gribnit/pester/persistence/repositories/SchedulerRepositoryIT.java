package net.gribnit.pester.persistence.repositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.zethmayr.benjamin.spring.common.exception.TestFailedException;
import net.zethmayr.benjamin.spring.common.repository.DefaultSchemaService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static net.gribnit.pester.TestConstants.exampleSchedulerDefinition;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class SchedulerRepositoryIT {
    @Autowired
    private SchedulerRepository underTest;

    @SpyBean
    private BasicSchedulerRepository basicSchedulers;

    @SpyBean
    private BasicSchedulerParameterRepository basicSchedulerParams;

    @Autowired
    private DefaultSchemaService schemaService;

    @Autowired
    private ObjectMapper om;

    @Before
    public void setUp() {
        schemaService.nuke(underTest, basicSchedulers, basicSchedulerParams);
        schemaService.applySchemaFor(underTest, basicSchedulers, basicSchedulerParams);
    }

    @Test
    public void canInsertThenRead() throws Exception {
        val original = exampleSchedulerDefinition();
        LOG.info("original is {} ({})", om.writeValueAsString(original), original);
        val id = underTest.insert(original);
        LOG.info("after insert, is {} ({})", om.writeValueAsString(original), original);
        val read = underTest.get(id).orElseThrow(TestFailedException::new);
        LOG.info("read is {} ({})", om.writeValueAsString(read), read);
        assertThat(read, is(original));
    }
}
