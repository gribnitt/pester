package net.gribnit.pester.persistence.repositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.gribnit.pester.domain.definitions.DeployedPesterDefinition;
import net.gribnit.pester.domain.definitions.UserDefinition;
import net.zethmayr.benjamin.spring.common.repository.DefaultSchemaService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static net.gribnit.pester.TestConstants.examplePesterDefinition;
import static net.gribnit.pester.TestConstants.exampleUserDefinition;
import static net.gribnit.pester.TestConstants.moreMethods;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isA;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class DeployedPesterRepositoryIT {
    @Autowired
    private DeployedPesterRepository underTest;

    @SpyBean
    private DeployedPesterParameterRepository pesterParameters;

    @Autowired
    private DefaultSchemaService schemaService;

    @Autowired
    private ObjectMapper om;

    @Before
    public void setUp() {
        schemaService.nuke(underTest, pesterParameters);
        schemaService.applySchemaFor(underTest, pesterParameters);
    }

    @Test
    public void wires() {
        assertThat(underTest, isA(DeployedPesterRepository.class));
    }

    @Test
    public void canInsertThenRead() throws Exception {
        val original = new DeployedPesterDefinition()
                .from(examplePesterDefinition()
                        .withParams(m -> m
                                .put("evil", 9)
                                .put("nice", -20)
                        )
                );
        LOG.info("original is {} ({})", om.writeValueAsString(original), original);
        val id = underTest.insert(original);
        LOG.info("after insert is {} ({})", om.writeValueAsString(original), original);
        assertThat(id, not(nullValue()));
        assertThat(id, is(original.getId()));
        val readReturn = underTest.get(id);
        assertThat(readReturn.isPresent(), is(true));
        val read = readReturn.get();
        LOG.info("read is {} ({})", om.writeValueAsString(read), read);
        assertThat(read, is(original));
    }

    @Test
    public void canInsertMultipleThenGetAll() throws Exception {
        for (int i = 0; i < 10; i++) {
            LOG.info("inserting {}", om.writeValueAsString(examplePesterDefinition()));
            underTest.insert(new DeployedPesterDefinition()
                    .from(examplePesterDefinition())
                    .setUserId(1)
            );
        }
        val read = underTest.getAll();
        LOG.info("read {}", om.writeValueAsString(read));
        assertThat(read, hasSize(10));
    }

}
