package net.gribnit.pester.persistence.repositories;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.zethmayr.benjamin.spring.common.exception.TestFailedException;
import net.zethmayr.benjamin.spring.common.repository.DefaultSchemaService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static net.gribnit.pester.TestConstants.exampleGeneratorDefinition;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class GeneratorRepositoryIT {
    @Autowired
    private GeneratorRepository underTest;

    @SpyBean
    private GeneratorTemplateRepository generatorTemplates;

    @Autowired
    private DefaultSchemaService schemaService;

    @Autowired
    private ObjectMapper om;

    @Before
    public void setUp() {
        schemaService.nuke(underTest, generatorTemplates);
        schemaService.applySchemaFor(underTest, generatorTemplates);
    }

    @Test
    public void canInsertThenRead() throws Exception {
        val original = exampleGeneratorDefinition();
        LOG.info("original is {} ({})", om.writeValueAsString(original), original);
        val id = underTest.insert(original);
        LOG.info("after insert {} ({})", om.writeValueAsString(original), original);
        val read = underTest.get(id).orElseThrow(TestFailedException::new);
        LOG.info("read is {} ({})", om.writeValueAsString(read), read);
        assertThat(read, is(original));
    }
}
