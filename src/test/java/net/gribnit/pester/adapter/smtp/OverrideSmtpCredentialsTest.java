package net.gribnit.pester.adapter.smtp;

import lombok.val;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class OverrideSmtpCredentialsTest {

    private static final String OVERRIDE_USER = "realuser@gmail.com";
    private static final String OVERRIDE_PASSWORD = "F00dPr0c355|";

    @Test
    public void canParseAndCallConfigSetters() throws Exception {
        val config = mock(MailtoHandlerConfig.class);
        val underTest = new OverrideSmtpCredentials(config);
        underTest.run("irrelevant", "bogus", "smtp",
                "username", OVERRIDE_USER,
                "password", OVERRIDE_PASSWORD, "derp", "wibble",
                "dump", "chaff", "waste", "pointless");
        verify(config).setPassword(OVERRIDE_PASSWORD);
        verify(config).setUsername(OVERRIDE_USER);
    }
}
