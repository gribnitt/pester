package net.gribnit.pester.adapter.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.gribnit.pester.domain.definitions.GeneratorDefinition;
import net.gribnit.pester.domain.definitions.GeneratorDefinitionList;
import net.gribnit.pester.domain.definitions.PesterDefinition;
import net.gribnit.pester.exception.CreationConflictException;
import net.gribnit.pester.exception.ModificationConflictException;
import net.gribnit.pester.exception.NotFoundException;
import net.gribnit.pester.application.service.PesterGeneratorsCrudService;
import net.zethmayr.benjamin.spring.common.util.ListBuilder;
import net.zethmayr.benjamin.spring.common.util.MapBuilder;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;

import static net.gribnit.pester.TestConstants.exampleGeneratorDefinition;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isA;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@Slf4j
public class PesterGeneratorsControllerTest {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ObjectMapper om;

    @MockBean
    private PesterGeneratorsCrudService controlled;

    @Autowired
    private PesterGeneratorsController underTest;

    private String root;

    @Before
    public void setUp() {
        reset(controlled);
        root = "http://localhost:" + port + "/pester/with";
        LOG.info("root is {}", root);
    }

    @Test
    public void wires() {
        assertThat(underTest, isA(PesterGeneratorsController.class));
    }

    @Test
    public void getCallsControlledForListToSerialize() throws IOException {
        final List<GeneratorDefinition> original = ListBuilder.array(
                exampleGeneratorDefinition().setId(1),
                exampleGeneratorDefinition().setId(2),
                exampleGeneratorDefinition().setId(3)
        ).build();
        doReturn(original).when(controlled).getAll();
        final ResponseEntity<String> response = restTemplate.getForEntity(
                root + "/",
                String.class,
                MapBuilder.hash().put("Accept", APPLICATION_JSON_UTF8_VALUE).build()
        );
        verify(controlled).getAll();
        val body = response.getBody();
        LOG.info("response body is {}", body);
        val reread = om.readValue(body, GeneratorDefinitionList.class);
        assertThat(reread, is(original));
    }

    @Test
    public void getForGeneratorCallsControlledForObjectToSerialize() throws Exception {
        final GeneratorDefinition original = exampleGeneratorDefinition().setId(1);
        doReturn(original).when(controlled).getOne("concern-troll");
        final ResponseEntity<String> response = restTemplate.getForEntity(
                root + "/concern-troll",
                String.class,
                MapBuilder.hash().put("Accept", APPLICATION_JSON_UTF8_VALUE).build()
        );
        verify(controlled).getOne("concern-troll");
        val body = response.getBody();
        LOG.info("response body is {}", body);
        val reread = om.readValue(body, GeneratorDefinition.class);
        assertThat(reread, is(original));
    }

    @Test
    public void getForGeneratorReturnsNotFoundIfControlledThrowsNotFound() throws Exception {
        doThrow(NotFoundException.missing("concern-troll")).when(controlled).getOne("concern-troll");
        final ResponseEntity<String> response = restTemplate.getForEntity(
                root + "/concern-troll",
                String.class,
                MapBuilder.hash().put("Accept", APPLICATION_JSON_UTF8_VALUE).build()
        );
        verify(controlled).getOne("concern-troll");
        assertThat(response.getStatusCode(), is(HttpStatus.NOT_FOUND));
        val body = response.getBody();
        LOG.info("response body is {}", body);
        assertThat(body, containsString("concern-troll"));
    }

    @Test
    public void deleteCallsControlled() throws Exception {
        restTemplate.delete(root + "/concern-troll");
        verify(controlled).delete("concern-troll");
    }

    @Test
    public void postCallsControlledForObjectToSerialize() throws Exception {
        val original = exampleGeneratorDefinition();
        assertThat(original, isA(GeneratorDefinition.class));
        final ResponseEntity<String> response = restTemplate.postForEntity(
                root + "/concern-troll",
                original,
                String.class,
                MapBuilder.hash().put("Accept", APPLICATION_JSON_UTF8_VALUE).build()
        );
        verify(controlled).create("concern-troll", original);
    }

    @Test
    public void postReturnsConflictMessageIfControlledThrowsConflict() throws Exception {
        val original = exampleGeneratorDefinition();
        doThrow(CreationConflictException.because("Testing")).when(controlled).create(any(),any());
        final ResponseEntity<String> response = restTemplate.postForEntity(
                root + "/concern-troll",
                original,
                String.class,
                MapBuilder.hash().put("Accept", APPLICATION_JSON_UTF8_VALUE).build()
        );
        assertThat(response.getStatusCode(), is(HttpStatus.CONFLICT));
        val body = response.getBody();
        LOG.info("response body is {}", body);
        assertThat(body, startsWith("Conflict"));
    }

    @Test
    public void putCallsControlled() throws Exception {
        val original = exampleGeneratorDefinition();
        restTemplate.put(root + "/concern-troll", original);
        verify(controlled).modify("concern-troll", original);
    }

    @Test
    public void putSomethingIfControlledThrows() throws Exception {
        val original = exampleGeneratorDefinition();
        doThrow(ModificationConflictException.because("Testing")).when(controlled).modify(any(), any());
        restTemplate.put(root + "/concern-troll", original);
        verify(controlled).modify("concern-troll", original);
        // that should have thrown... maybe it did? but delete does not report errors.
    }

    @Test
    public void patchCallsControlledForObjectToSerialize() throws Exception {
        val original = exampleGeneratorDefinition();
        val response = restTemplate.patchForObject(root + "/concern-troll", original, PesterDefinition.class);
        verify(controlled).modify("concern-troll", original);
    }

    @Test
    public void patchFailsIfControlledThrows() throws Exception {
        thrown.expectMessage(containsString("Conflict"));
        val original = exampleGeneratorDefinition();
        doThrow(ModificationConflictException.because("Testing")).when(controlled).modify(any(), any());
        restTemplate.patchForObject(root + "/concern-troll", original, PesterDefinition.class);
    }
}
