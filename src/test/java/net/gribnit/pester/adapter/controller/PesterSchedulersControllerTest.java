package net.gribnit.pester.adapter.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.gribnit.pester.domain.definitions.PesterDefinition;
import net.gribnit.pester.domain.definitions.SchedulerDefinition;
import net.gribnit.pester.domain.definitions.SchedulerDefinitionList;
import net.gribnit.pester.application.service.PesterSchedulersCrudService;
import net.zethmayr.benjamin.spring.common.util.ListBuilder;
import net.zethmayr.benjamin.spring.common.util.MapBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static net.gribnit.pester.TestConstants.exampleSchedulerDefinition;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isA;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@Slf4j
public class PesterSchedulersControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ObjectMapper om;

    @MockBean
    private PesterSchedulersCrudService controlled;

    @Autowired
    private PesterSchedulersController underTest;

    private String root;

    @Before
    public void setUp() {
        reset(controlled);
        root = "http://localhost:" + port + "/pester/by";
        LOG.info("root is {}", root);
    }

    @Test
    public void wires() {
        assertThat(underTest, isA(PesterSchedulersController.class));
    }

    @Test
    public void getCallsControlledForListToSerialize() throws IOException {
        val original = ListBuilder.array(
                exampleSchedulerDefinition().setId(1),
                exampleSchedulerDefinition().setId(2),
                exampleSchedulerDefinition().setId(3)
        ).build();
        doReturn(original).when(controlled).getAll();
        final ResponseEntity<String> response = restTemplate.getForEntity(
                root + "/",
                String.class,
                MapBuilder.hash().put("Accept", APPLICATION_JSON_UTF8_VALUE).build()
        );
        verify(controlled).getAll();
        val body = response.getBody();
        LOG.info("response body is {}", body);
        val reread = om.readValue(body, SchedulerDefinitionList.class);
        assertThat(reread, is(original));
    }

    @Test
    public void getForSchedulerCallsControlledForObjectToSerialize() throws Exception {
        val original = exampleSchedulerDefinition().setId(1);
        doReturn(original).when(controlled).getOne("Joe");
        final ResponseEntity<String> response = restTemplate.getForEntity(
                root + "/Joe",
                String.class,
                MapBuilder.hash().put("Accept", APPLICATION_JSON_UTF8_VALUE).build()
        );
        verify(controlled).getOne("Joe");
        val body = response.getBody();
        LOG.info("response body is {}", body);
        val reread = om.readValue(body, SchedulerDefinition.class);
        assertThat(reread, is(original));
    }

    @Test
    public void deleteCallsControlled() throws Exception {
        restTemplate.delete(root + "/Joe");
        verify(controlled).delete("Joe");
    }

    @Test
    public void postCallsControlledForObjectToSerialize() throws Exception {
        val original = exampleSchedulerDefinition();
        assertThat(original, isA(SchedulerDefinition.class));
        final ResponseEntity<String> response = restTemplate.postForEntity(
                root + "/Joe",
                original,
                String.class,
                MapBuilder.hash().put("Accept", APPLICATION_JSON_UTF8_VALUE).build()
        );
        verify(controlled).create("Joe", original);
    }

    @Test
    public void putCallsControlled() throws Exception {
        val original = exampleSchedulerDefinition();
        restTemplate.put(root + "/Joe", original);
        verify(controlled).modify("Joe", original);
    }

    @Test
    public void patchCallsControlledForObjectToSerialize() throws Exception {
        val original = exampleSchedulerDefinition();
        val response = restTemplate.patchForObject(root + "/Joe", original, PesterDefinition.class);
        verify(controlled).modify("Joe", original);
    }
}
