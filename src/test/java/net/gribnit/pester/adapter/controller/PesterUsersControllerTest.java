package net.gribnit.pester.adapter.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.gribnit.pester.domain.definitions.PesterDefinition;
import net.gribnit.pester.domain.definitions.UserDefinition;
import net.gribnit.pester.domain.definitions.UserDefinitionList;
import net.gribnit.pester.application.service.PesterUsersCrudService;
import net.zethmayr.benjamin.spring.common.util.ListBuilder;
import net.zethmayr.benjamin.spring.common.util.MapBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static net.gribnit.pester.TestConstants.exampleUserDefinition;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isA;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@Slf4j
public class PesterUsersControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ObjectMapper om;

    @MockBean
    private PesterUsersCrudService controlled;

    @Autowired
    private PesterUsersController underTest;

    private String root;

    @Before
    public void setUp() {
        reset(controlled);
        root = "http://localhost:" + port + "/pester/users";
        LOG.info("root is {}", root);
    }

    @Test
    public void wires() {
        assertThat(underTest, isA(PesterUsersController.class));
    }

    @Test
    public void getCallsControlledForListToSerialize() throws IOException {
        val original = ListBuilder.array(
                new UserDefinition().setId(1).from(exampleUserDefinition()),
                new UserDefinition().setId(2).from(exampleUserDefinition()),
                new UserDefinition().setId(3).from(exampleUserDefinition())
        ).build();
        doReturn(original).when(controlled).getAll();
        final ResponseEntity<String> response = restTemplate.getForEntity(
                root + "/",
                String.class,
                MapBuilder.hash().put("Accept",APPLICATION_JSON_UTF8_VALUE).build()
        );
        verify(controlled).getAll();
        val body = response.getBody();
        LOG.info("response body is {}", body);
        val reread = om.readValue(body, UserDefinitionList.class);
        assertThat(reread, is(original));
    }

    @Test
    public void getForUserCallsControlledForObjectToSerialize() throws Exception {
        val original = new UserDefinition().from(exampleUserDefinition()).setId(1);
        doReturn(original).when(controlled).getOne("Joe");
        final ResponseEntity<String> response = restTemplate.getForEntity(
                root + "/Joe",
                String.class,
                MapBuilder.hash().put("Accept",APPLICATION_JSON_UTF8_VALUE).build()
        );
        verify(controlled).getOne("Joe");
        val body = response.getBody();
        LOG.info("response body is {}", body);
        val reread = om.readValue(body, UserDefinition.class);
        assertThat(reread, is(original));
    }

    @Test
    public void deleteCallsControlled() throws Exception {
        restTemplate.delete(root + "/Joe");
        verify(controlled).delete("Joe");
    }

    @Test
    public void postCallsControlledForObjectToSerialize() throws Exception {
        val original = exampleUserDefinition();
        assertThat(original, isA(UserDefinition.class));
        final ResponseEntity<String> response = restTemplate.postForEntity(
                root + "/Joe",
                original,
                String.class,
                MapBuilder.hash().put("Accept", APPLICATION_JSON_UTF8_VALUE).build()
        );
        verify(controlled).create("Joe", original);
    }

    @Test
    public void putCallsControlled() throws Exception {
        val original = exampleUserDefinition();
        restTemplate.put(root + "/Joe", original);
        verify(controlled).modify("Joe", original);
    }

    @Test
    public void patchCallsControlledForObjectToSerialize() throws Exception {
        val original = exampleUserDefinition();
        val response = restTemplate.patchForObject( root + "/Joe", original, PesterDefinition.class);
        verify(controlled).modify("Joe", original);
    }
}
