package net.gribnit.pester.adapter.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import net.gribnit.pester.domain.definitions.DeployedPesterDefinition;
import net.gribnit.pester.domain.definitions.DeployedPesterDefinitionList;
import net.gribnit.pester.domain.definitions.PesterDefinition;
import net.gribnit.pester.domain.definitions.PesterDefinitionList;
import net.gribnit.pester.application.service.PesterDeploymentCrudService;
import net.zethmayr.benjamin.spring.common.util.ListBuilder;
import net.zethmayr.benjamin.spring.common.util.MapBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static net.gribnit.pester.TestConstants.examplePesterDefinition;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isA;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@Slf4j
public class PesterDeploymentControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private ObjectMapper om;

    @MockBean
    private PesterDeploymentCrudService controlled;

    @Autowired
    private PesterDeploymentController underTest;

    private String root;

    @Before
    public void setUp() {
        reset(controlled);
        root = "http://localhost:" + port + "/pester/at";
        LOG.info("root is {}", root);
    }

    @Test
    public void wires() {
        assertThat(underTest, isA(PesterDeploymentController.class));
    }

    @Test
    public void getCallsControlledForListToSerialize() throws IOException {
        val original = ListBuilder.array(
                new DeployedPesterDefinition().setUserId(1).from(examplePesterDefinition()),
                new DeployedPesterDefinition().setUserId(2).from(examplePesterDefinition()),
                new DeployedPesterDefinition().setUserId(3).from(examplePesterDefinition())
        ).build();
        doReturn(original).when(controlled).getAll();
        final ResponseEntity<String> response = restTemplate.getForEntity(
                root + "/",
                String.class,
                MapBuilder.hash().put("Accept",APPLICATION_JSON_UTF8_VALUE).build()
        );
        verify(controlled).getAll();
        val body = response.getBody();
        LOG.info("response body is {}", body);
        val reread = om.readValue(body, DeployedPesterDefinitionList.class);
        assertThat(reread, is(original));
    }

    @Test
    public void getForUserCallsControlledForListToSerialize() throws Exception {
        val original = ListBuilder.array(
                new PesterDefinition().from(examplePesterDefinition()).setId(1),
                new PesterDefinition().from(examplePesterDefinition()).setId(2),
                new PesterDefinition().from(examplePesterDefinition()).setId(3)
        ).build();
        doReturn(original).when(controlled).getForUser("Joe");
        final ResponseEntity<String> response = restTemplate.getForEntity(
                root + "/Joe",
                String.class,
                MapBuilder.hash().put("Accept",APPLICATION_JSON_UTF8_VALUE).build()
        );
        verify(controlled).getForUser("Joe");
        val body = response.getBody();
        LOG.info("response body is {}", body);
        val reread = om.readValue(body, PesterDefinitionList.class);
        assertThat(reread, is(original));
    }

    @Test
    public void deleteCallsControlled() throws Exception {
        restTemplate.delete(root + "/1");
        verify(controlled).delete(1);
    }

    @Test
    public void postCallsControlledForObjectToSerialize() throws Exception {
        val original = new PesterDefinition<>()
                .setGenerator("panic-attack")
                .setScheduler("heavy-shelling")
                .withParams(p -> p.put("boss-email", "boss@sch.mo"));
        assertThat(original, isA(PesterDefinition.class));
        final ResponseEntity<String> response = restTemplate.postForEntity(
                root + "/Joe",
                original,
                String.class,
                MapBuilder.hash().put("Accept", APPLICATION_JSON_UTF8_VALUE).build()
        );
        verify(controlled).deploy("Joe", original);
    }

    @Test
    public void putCallsControlled() throws Exception {
        val original = examplePesterDefinition();
        restTemplate.put(root + "/1", original);
        verify(controlled).modify(1, original);
    }

    @Test
    public void patchCallsControlledForObjectToSerialize() throws Exception {
        val original = examplePesterDefinition();
        val response = restTemplate.patchForObject( root + "/1", original, PesterDefinition.class);
        verify(controlled).modify(1, original);
    }
}
