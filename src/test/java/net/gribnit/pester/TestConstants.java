package net.gribnit.pester;

import net.gribnit.pester.domain.definitions.BasicSchedulerDefinition;
import net.gribnit.pester.domain.definitions.ContactMethodDefinition;
import net.gribnit.pester.domain.definitions.ContactMethodDefinitionList;
import net.gribnit.pester.domain.definitions.GeneratorDefinition;
import net.gribnit.pester.domain.definitions.PesterDefinition;
import net.gribnit.pester.domain.definitions.SchedulerDefinition;
import net.gribnit.pester.domain.definitions.UserDefinition;
import net.gribnit.pester.utility.StringList;
import net.zethmayr.benjamin.spring.common.util.ListBuilder;

import java.util.function.Consumer;

public final class TestConstants {
    private TestConstants() {
    }

    public static final String TEST_PROFILE = "test";

    public static final String QUOTE = "\"";

    public static final String EXAMPLE_SCHEDULER = "damn-you-joe";
    public static final String EXAMPLE_GENERATOR = "passive-aggressive";

    public static String quoted(final String toQuote) {
        return QUOTE + toQuote + QUOTE;
    }

    public static PesterDefinition<PesterDefinition> examplePesterDefinition() {
        return new PesterDefinition<>()
                .setScheduler(EXAMPLE_SCHEDULER)
                .setGenerator(EXAMPLE_GENERATOR);
    }

    public static UserDefinition exampleUserDefinition() {
        return new UserDefinition()
                .setId(1)
                .setName("Joe")
                .setContactMethods(ListBuilder.on(new ContactMethodDefinitionList()).add(
                        new ContactMethodDefinition().setName("email").setUrl("mailto://joe@sch.mo").withParams((m) -> m.put("default", true))
                ).build());
    }

    public static Consumer<ListBuilder<ContactMethodDefinition, ContactMethodDefinitionList>> moreMethods() {
        return l -> l.add(
                        new ContactMethodDefinition().setName("im").setUrl("whateverIm://handle").withParams((m) -> m.put("default", true)),
                        new ContactMethodDefinition().setName("moreim").setUrl("bleetr://handleAtNite"),
                        new ContactMethodDefinition().setName("poke").setUrl("desk://13C43").withParams((m) -> m.put("default", true).put("transit", "10M"))
                );
    }

    public static SchedulerDefinition exampleSchedulerDefinition() {
        return new SchedulerDefinition()
                .setId(1)
                .setName("damn-you-joe")
                .setGenerator("panic-attack")
                .withSchedulers((l) -> l.add(
                        new BasicSchedulerDefinition().setType("periodic").withParams((m) -> m
                                .put("method", "_random")
                                .put("timing", "random-hourly")
                        ),
                        new BasicSchedulerDefinition().setType("periodic").withParams((m) -> m
                                .put("method","poke")
                                .put("timing", "daily@11:47")
                        ),
                        new BasicSchedulerDefinition().setType("periodic").withParams((m) -> m
                                .put("method", "moreIm")
                                .put("timing","daily@16:50")
                        ),
                        new BasicSchedulerDefinition().setType("periodic").withParams((m) -> m
                                .put("method","mailto://boss@sch.mo")
                                .put("timing", "random-weekly")
                        ).setGenerator("concern-troll")
                ));
    }

    public static GeneratorDefinition exampleGeneratorDefinition() {
        return concernTroll();
    }

    public static GeneratorDefinition concernTroll() {
        return new GeneratorDefinition()
                .setId(1)
                .setName("concern-troll")
                .setTemplates(
                        StringList.builder().add(
                                "Hey, is {user} alright? They looked really tired the last time I saw them",
                                "Been trying to get hold of {user}, have you seen them today?"
                        ).build()
                );
    }

    public static GeneratorDefinition panicAttack() {
        return new GeneratorDefinition()
                .setName("panic-attack")
                .setTemplates(
                        StringList.builder().add(
                               "{user}, I really need {subject} today or all our butts are on the line",
                               "Please, I'm begging you, can you get to {subject} today?",
                               "Help! {user}, I need {subject} before COB!!!"
                        ).build()
                );
    }
}
